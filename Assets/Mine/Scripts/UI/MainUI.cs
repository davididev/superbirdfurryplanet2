﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainUI : MonoBehaviour
{
	private static float Enemy_HP_Perc = 0f;
	private static float enemyHealthTimer = 0f;
	public static float AirPerc = -1f;
	public static bool IsNPCAvailable = false;
	public static string WorldName = "";
	public GameObject enemyHealthHolder, airHolder;
	public Image enemyHealthBar, playerHealthBar, playerMagicBar, airBar;
	public Animator transitionAnim;
	
	public TMPro.TextMeshProUGUI playerHealthText, playerMagicText, niaraText, playerFeathersText;
	
	public static bool UpdateUI = true;
	
	public static void LoadScene(string sceneName, int transitionID, Vector3 newPosition, AudioClip snd = null)
	{
		
		MainUI inst = GameObject.FindWithTag("MainUI").GetComponent<MainUI>();
		if(snd != null)
			AudioSource.PlayClipAtPoint(snd, Camera.main.transform.position);
		inst.StartCoroutine(inst.Transition(sceneName, transitionID, newPosition));
	}
	
	IEnumerator Transition(string sceneName, int animationID, Vector3 newPosition)
	{
		PlayerMovement.IsFrozen = true;
		AsyncOperation op = SceneManager.LoadSceneAsync(sceneName);
		op.allowSceneActivation = false;
		
		transitionAnim.SetTrigger("Anim" + animationID);
		yield return new WaitForSecondsRealtime(2.5f);
		op.allowSceneActivation = true;
		while(op.isDone == false)
		{
			yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);
		}
		CharacterController cc = GameObject.FindWithTag("Player").GetComponent<CharacterController>();
		cc.enabled = false;
		cc.transform.position = newPosition;
		yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);
		cc.enabled = true;
		transitionAnim.SetTrigger("Close");
		PlayerMovement.IsFrozen = false;
	}
	
	[System.Serializable]
	public class TextFlip
	{
		public TMPro.TextMeshProUGUI textObj;
		protected string lastText;
		
		public TextFlip()
		{
			lastText = "";
		}
		
		public void SetStr(string s)
		{
			if(s != lastText)
			{
				lastText = s;
				textObj.text = s;
				textObj.transform.localEulerAngles = new Vector3(90f, 0f, 0f);
				iTween.RotateTo(textObj.gameObject, iTween.Hash("islocal", true, "rotation", Vector3.zero, "time", 1f, "ignoretimescale", true));
			}
		}
	}
	
	[SerializeField] public TextFlip magicFlip, pauseFlip, talkText;
	
	
	
	
	public static void SetEnemyPerc(float p)
	{
		Enemy_HP_Perc = p;
		enemyHealthTimer = 4f;
	}
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		//Update enemy health
        if(enemyHealthTimer > 0f)
		{
			enemyHealthHolder.SetActive(true);
			enemyHealthBar.fillAmount = Enemy_HP_Perc;
			enemyHealthTimer -= Time.deltaTime;
			if(enemyHealthTimer <= 0f)
			{
				enemyHealthHolder.SetActive(false);
			}
		}
		if(UpdateUI)
		{
			playerHealthText.text = GameDataHolder.instance.MinHP + "/" + GameDataHolder.instance.MaxHP;
			playerMagicText.text = GameDataHolder.instance.MinMP + "/" + GameDataHolder.instance.MaxMP;
			playerHealthBar.fillAmount = (GameDataHolder.instance.MinHP * 100 / GameDataHolder.instance.MaxHP) / 100f;
			playerMagicBar.fillAmount = (GameDataHolder.instance.MinMP * 100 / GameDataHolder.instance.MaxMP) / 100f;
			playerFeathersText.text = GameDataHolder.instance.MinFeathers + "/" + GameDataHolder.instance.MaxFeathers;
			
			
			
			niaraText.text = "x" + GameDataHolder.instance.Niara;
			niaraText.color = (GameDataHolder.instance.Niara >= GameDataHolder.instance.MaxNiara) ? Color.yellow : Color.white;
			
			string element = "Telekinesis";
			int ele = FindObjectOfType<SwordController>().elementID;
			if(ele == DmgMessage.ELEMENT_FIRE)
				element = "Fireball";
			if(ele == DmgMessage.ELEMENT_ICE)
				element = "Ice Lance";
			if(ele == DmgMessage.ELEMENT_SHADOW)
				element = "Dark Bomb";
			if(ele == DmgMessage.ELEMENT_LIGHTNING)
				element = "Lightning";
			if(ele == DmgMessage.ELEMENT_COSMIC)
				element = "Northern Lights";
			magicFlip.SetStr(element);
			
			if(IsNPCAvailable)
				talkText.SetStr("Talk");
			else
			{
				if(WorldName == "")
					talkText.SetStr("---");
				else
					talkText.SetStr(WorldName);
			}
			
			airHolder.SetActive(!Mathf.Approximately(AirPerc, -1f));
			airBar.fillAmount = AirPerc;
			
		}
    }
}
