﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
	public int TouchDamage = 1;
	public enum BRAIN { Empty, Wander, Follow, Back_To_Right, Back_To_Left };
	private Coroutine currentBrain;
	
	protected CharacterControllerMovement ccm;
	protected EnemyHealth health;

	protected Animator anim;
	
    // Start is called before the first frame update
    void OnEnable()
    {
		firstBrain = true;
        FirstUpdate();
		StartCoroutine(MainRoutine());
		if(anim == null)
			anim = GetComponent<Animator>();
		if(ccm == null)
			ccm = GetComponent<CharacterControllerMovement>();
		if(health == null)
			health = GetComponent<EnemyHealth>();
    }
	
	public virtual void FirstUpdate()
	{
	}
	
	public virtual IEnumerator MainRoutine()
	{
		yield return null;
	}

    // Update is called once per frame
    void Update()
    {
		if(ccm == null)
			ccm = GetComponent<CharacterControllerMovement>();
		if(health == null)
			health = GetComponent<EnemyHealth>();
		if(anim == null)
			anim = GetComponent<Animator>();
		
		anim.SetFloat("MoveSpeed", ccm.Velocity.sqrMagnitude);
		
		
		if(transform.position.y < MapVars.DeathY)
			gameObject.SetActive(false);
        OnUpdate();
    }
	
	void OnDisable()
	{
		StopCoroutine(currentBrain);
	}
	
	public virtual void OnUpdate()
	{
	}
	
	bool firstBrain = true;
	public void SetBrain(BRAIN b)
	{
		if(!firstBrain)
			StopCoroutine(currentBrain);
		
		firstBrain = false;
		if(b == BRAIN.Empty)
			StopCoroutine(currentBrain);
		if(b == BRAIN.Wander)
			currentBrain = StartCoroutine(WanderRoutine());
		if(b == BRAIN.Follow)
			currentBrain = StartCoroutine(FollowRoutine());
	}
	
	private IEnumerator WanderRoutine()
	{
		Vector3 startingPos = transform.position;
		while(gameObject)
		{
			Vector3 targetPos = startingPos;
			targetPos.x += Random.Range(-5f, 5f);
			targetPos.z += Random.Range(-5f, 5f);
			float t = 1f;
			while(t > 0f)
			{
				ccm.RotateTowardsPoint(targetPos);
				t -= Time.deltaTime;
				yield return new WaitForEndOfFrame();
			}
			ccm.MoveVec.y = 1f;
			while(Vector3.Distance(transform.position, targetPos) < 1f)
			{
				yield return new WaitForEndOfFrame();
			}
			
		}
		
	}
	
	private IEnumerator FollowRoutine()
	{
		while(gameObject)
		{
			Vector3 targetPos = PlayerMovement.Position;
			//Debug.Log("Follow routine " + targetPos);
			ccm.RotateTowardsPoint(targetPos);
			ccm.MoveVec.y = 1f;
			yield return new WaitForEndOfFrame();
			
		}
		
	}
	
	void OnControllerColliderHit(ControllerColliderHit hit)
	{
		if(hit.gameObject.tag == "Player")
		{
			hit.gameObject.SendMessage("OnDamage", new DmgMessage(TouchDamage, 0, transform.position, hit.normal));
		}
		
	}
}
