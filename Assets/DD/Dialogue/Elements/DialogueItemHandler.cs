﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class DialogueItemHandler : DialogueItem
{
    public string itemToAlter = "1A";
    public enum ALTER_ITEM { GET_COUNT, ADD, SUBTRACT};
    public ALTER_ITEM alterType = ALTER_ITEM.GET_COUNT;

    public override IEnumerator Run()
    {
        yield return base.Run();
        if (alterType == ALTER_ITEM.GET_COUNT)
        {
			float f = 0f;
			for(int i = 0; i < GameDataHolder.instance.items.Length; i++)
			{
				if(GameDataHolder.instance.items[i] == itemToAlter)
					f++;
			}

            DialogueHandler.SetVar("%itemCount", f);
        }
        if(alterType == ALTER_ITEM.ADD)
		{
			for(int i = 0; i < GameDataHolder.instance.items.Length; i++)
			{
				bool failure = true;
				if(GameDataHolder.instance.items[i] == "0")
				{
					GameDataHolder.instance.items[i] = itemToAlter;
					failure = false;
					break;
				}
				
				DialogueHandler.SetVar("%failure", failure ? 1f : 0f);
			}
		}
		if(alterType == ALTER_ITEM.SUBTRACT)
		{
			for(int i = 0; i < GameDataHolder.instance.items.Length; i++)
			{
				if(GameDataHolder.instance.items[i] == itemToAlter)
				{
					GameDataHolder.instance.items[i] = "0";
					break;
				}
			}
		}
        completed = true;
        yield return null;
    }

    public override void DrawEditor(Rect verticalGroup)
    {
        GUILayout.Label("Item to modify:");
        itemToAlter = GUILayout.TextField(itemToAlter);
                
		//alterType = (DialogueSetVar.ALTER_ITEM) this.EnumPopup("Alter by", alterType);
		
		
		
		GUILayout.Label("Alter Type: " +alterType.ToString());
		
		
		
		GUILayout.BeginHorizontal();
		if(alterType != ALTER_ITEM.GET_COUNT)
		{
			if(GUILayout.Button("Get Count"))
				alterType = ALTER_ITEM.GET_COUNT;
		}
		if(alterType != ALTER_ITEM.ADD)
		{
			if(GUILayout.Button("Add"))
				alterType = ALTER_ITEM.ADD;
		}
		if(alterType != ALTER_ITEM.SUBTRACT)
		{
			if(GUILayout.Button("Subtract"))
				alterType = ALTER_ITEM.SUBTRACT;
		}
		
		GUILayout.EndHorizontal();
		
		if(alterType == ALTER_ITEM.GET_COUNT)
			GUILayout.Label("This item will be stored in %itemCount.");
		if(alterType == ALTER_ITEM.ADD)
			GUILayout.Label("This will create a %failure item.");
		if(alterType == ALTER_ITEM.SUBTRACT)
			GUILayout.Label("Make sure to check count first!");
    }

    public override string ToString()
    {
        return "Item Handler (" + alterType.ToString() + ")";
    }
}
