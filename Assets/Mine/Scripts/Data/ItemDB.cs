﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDB 
{
    public static ItemDB instance = new ItemDB();
	public enum SPECIAL {None, Beserk, Revive};
	
	public class Info
	{
		public int RestoreHP = 0;
		public int RestoreMP = 0;
		public string ItemName = "";
		public string ItemDescription = "";
		
		public SPECIAL SpecialPowers = SPECIAL.None;
		
		public Info(int hp, int mp, string itn, string itd, SPECIAL sp = SPECIAL.None)
		{
			RestoreHP = hp;
			RestoreMP = mp;
			ItemName = itn;
			ItemDescription = itd;
			SpecialPowers = sp;
		}
	}
	
	public static string GetItemString(string itemID)
	{
		string s = "";
		Info info;
		if(db.TryGetValue(itemID, out info))
		{
			s = "<color=green>" + info.ItemName + "</color>: " + info.ItemDescription; 
		}
		return s;
	}
	
	public static int GetItemHP(string itemID)
	{
		Info info;
		if(db.TryGetValue(itemID, out info))
		{
			return info.RestoreHP;
		}
		return -1;
	}
	
	public static int GetItemMP(string itemID)
	{
		Info info;
		if(db.TryGetValue(itemID, out info))
		{
			return info.RestoreMP;
		}
		return -1;
	}
	
	public static ItemDB.SPECIAL GetItemSpecial(string itemID)
	{
		Info info;
		if(db.TryGetValue(itemID, out info))
		{
			return info.SpecialPowers;
		}
		return ItemDB.SPECIAL.None;
	}
	
	public static Dictionary<string, Info> db;
	
	
	public ItemDB()
	{
		db = new Dictionary<string, Info>();
		
		//Global Items
		db.Add("0", new Info(0, 0, "Empty", "Go to a shop and buy something to fill this slot!"));
		db.Add("A", new Info(0, 0, "Ankh", "Keep on hand.  Brings you back to life if you die.", SPECIAL.Revive));
		db.Add("B", new Info(0, 0, "Power Juice", "Invokes beserk mode.", SPECIAL.Beserk));
		
		//World 1 items
		db.Add("1A", new Info(0, 10, "Coffee", "Delicious coffee that restores 10 MP."));
		db.Add("1B", new Info(7, 0, "Pepper", "It's fresh and restores 7 HP."));
		
	}
	
}
