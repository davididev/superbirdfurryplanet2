﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageIndicator : MonoBehaviour
{
	public TMPro.TextMeshProUGUI amountText;
	public Image plusOrXImage;
	
	public Sprite plusSprite, xSprite;
	public Animator anim;
	
    // Start is called before the first frame update
    void Display(int amt)
    {
		transform.LookAt(Camera.main.transform.position);
        if(amt < 0)
		{
			amountText.text = Mathf.Abs(amt).ToString();
			plusOrXImage.sprite = plusSprite;
		}
		else
		{
			amountText.text = Mathf.Abs(amt).ToString();
			plusOrXImage.sprite = xSprite;
		}
		
		anim.SetTrigger("Fade");
		Invoke("Hide", 1f);
    }
	
	void Hide()
	{
		gameObject.SetActive(false);
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
