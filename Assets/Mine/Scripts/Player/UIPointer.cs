﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPointer : MonoBehaviour
{
    public MeshRenderer rend;
    public static bool IsActive = false;

    public static Vector3 HandRotation { get; private set; }
	
	public Transform scanPoint, crosshair;
	
	private int layerMask;
	
    // Start is called before the first frame update
    void OnEnable()
    {
		layerMask = LayerMask.GetMask("UI");
        IsActive = true;
    }

    private void OnDisable()
    {
        IsActive = false;

    }

    // Update is called once per frame
    void Update()
    {
        HandRotation = transform.eulerAngles;
        rend.enabled = (Time.timeScale < 0.05f);

		crosshair.gameObject.SetActive(Time.timeScale < 0.05f);
        //Is paused
        if(Time.timeScale < 0.05f)
        {
			
            //Do a raycast for buttons and stuff
			RaycastHit info;
			if(Physics.Raycast(scanPoint.position, scanPoint.forward, out info, 50f, layerMask))
			{
				info.collider.gameObject.SendMessage("OnHighlight", info.point, SendMessageOptions.DontRequireReceiver);
				crosshair.position = info.point;
				crosshair.LookAt(Camera.main.transform.position);
			}
			else
			{
				crosshair.position = scanPoint.position + (scanPoint.forward * 50f);
				crosshair.LookAt(Camera.main.transform.position);
			}
        }
		
    }
}
