﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControllerMovement : MonoBehaviour
{
    public const float GRAVITY = 32.1522f;
    public float MinMoveSpeed = 0.5f, MaxMoveSpeed = 0.25f, SlowDownPerSecond = 2f, MoveAcceleration = 5f;
    private float forwardSpeed = 0f, rightSpeed = 0f;
    private bool jumping = false;
    private float jumpDestinationY = 0f;
    public bool SuspendMoveSpeedInAir = true, SlideOnSlopes = false;

	public float rotateSpeed = 180f;
    private float gravity = 0f;
    public bool IsGrounded { get; private set; }

	public float WaterVertical = 0f;
	public bool IsInWater { get; private set; }
	public bool IsUnderWater { get; private set; }

    public Vector2 MoveVec;
	[HideInInspector] public Vector3 Center;
    public float GravityScale = 1f;

    [HideInInspector] public CharacterController cc;
    public Vector2 Velocity
    {
        get { return new Vector2(rightSpeed, forwardSpeed); }
    }

    public class CCForce
    {
        private float MAX_TIME = 0.5f;
        private float time = 0f;
        private Vector3 force;

        /// <summary>
        /// Create a force, as it sounds
        /// </summary>
        /// <param name="f">The vector of the force</param>
        public void CreateForce(Vector3 f, float mt = 0.5f)
        {
            force = f;
            time = mt;
            MAX_TIME = mt;
        }

        /// <summary>
        /// Utility function called per step.
        /// </summary>
        /// <returns></returns>
        public Vector3 ForcePerSecond()
        {
            time -= Time.deltaTime;
            if (time < 0f)
            {
                time = 0f;
                return Vector3.zero;
            }
            Vector3 v = force * (time / MAX_TIME);
            v = v * Time.deltaTime;
            //Debug.Log("Force: " + v);
            return v;
        }

        /// <summary>
        /// Checks to see if the force is empty.
        /// </summary>
        /// <returns></returns>
        public bool isEmpty()
        {
            if (time <= 0f)
                return true;
            else
                return false;
        }
    }

    /// <summary>
    /// Jump regardless of whether or not you are grounded
    /// </summary>
    /// <param name="dist">Jump Height</param>
    public void Jump(float dist)
    {
        gravity = 0f;
        jumpDestinationY = transform.position.y + dist;
        jumping = true;
    }

    /// <summary>
    /// Runs "Jump" if grounded.
    /// </summary>
    /// <param name="dist"></param>
    public void JumpIfGrounded(float dist)
    {
        if (IsGrounded)
            Jump(dist);
    }

    /// <summary>
    /// Add a force to this Character Controller Movement
    /// </summary>
    /// <param name="amt">Amount of the force</param>
    /// <param name="t">How long it should last (default is half a second)</param>
    public void AddForce(Vector3 amt, float t = 0.5f)
    {
        for(int i = 0; i < forces.Length; i++)
        {
            if(forces[i].isEmpty())
            {
                forces[i].CreateForce(amt, t);
                break;
            }
        }
    }


    private CCForce[] forces = new CCForce[6];
    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < forces.Length; i++)
        {
            forces[i] = new CCForce();
        }
    }
	
	//Should be called when you bounce of walls or otherwise need to stop move vecs
	public void ClearMoveVec()
	{
		forwardSpeed = 0f;
		rightSpeed = 0f;
	}

    // Update is called once per frame
    void Update()
    {
        if (cc == null)
            cc = GetComponent<CharacterController>();
		if(DialogueHandler.IS_RUNNING)
			return;
		Center = transform.position + cc.center;

        Vector3 movement = Vector3.zero;
        IsGrounded = cc.isGrounded;
        movement = ProcessGravity(movement);
        movement = ProcessMoveVec(movement);
		movement = ProcessSlope(movement);
        //Process forces.
        for(int i = 0; i < forces.Length; i++)
        {
            movement += forces[i].ForcePerSecond();
        }

        cc.Move(movement);
    }

	private float slopeTime = 0f;
	private Vector3 dragSlope;
	
	private float lastSlope = 0f;
	
	Vector3 ProcessSlope(Vector3 movement)
	{
		if(SlideOnSlopes)
		{
			if(IsGrounded)
			{
				RaycastHit info;
				if (Physics.Raycast(Center, Vector3.down, out info, cc.radius + 1.5f, LayerMask.GetMask("Default", "Enemy", "Player", "Platform")))
				{
					//if(info.normal.y < 0.9f)
					//{
						lastSlope = 0.1f;
						float slopeAngle = (1f - info.normal.y) * 360f;
						float angForce = slopeAngle / 45f * MaxMoveSpeed * 32.0f;
						Vector3 temp = Vector3.Cross(info.normal, Vector3.down);
						Vector3 v = Vector3.Cross(temp, info.normal) * angForce * Time.deltaTime;
						v.y = -0.5f;
						movement += v * Time.deltaTime;
						dragSlope = v;
					//}
					
					
				}
				else
				{
					if(lastSlope > 0f)
					{
						lastSlope -= Time.deltaTime;
						if(dragSlope.sqrMagnitude < 1f)
							movement += transform.forward * 5f * Time.deltaTime;
						else
							movement += dragSlope * 5f * Time.deltaTime;
					}
				}
					
			}
		}
		return movement;
	}

	float splashFXTimer = 0f;
    Vector3 ProcessGravity(Vector3 movement)
    {
		if(splashFXTimer > 0f)
			splashFXTimer -= Time.deltaTime;
        //Check for water
		RaycastHit info;
		if (Physics.Raycast((Center + (Vector3.down * 0.1f)), Vector3.up, out info, 200f, LayerMask.GetMask("Water")))
		{
			if(!IsInWater && splashFXTimer <= 0f)  //First step, make a splash!
			{
				splashFXTimer = 0.5f;
				GameObjectPool.GetInstance("Splash", info.point, Quaternion.identity);
			}
			float yDist = info.point.y - Center.y;
			IsInWater = true;
			
			IsUnderWater = (Mathf.Abs(yDist) > 0.1f);
			if(WaterVertical == 0f)
				gravity = Mathf.MoveTowards(gravity, 0f, WaterVertical * MoveAcceleration * Time.deltaTime);
			else
				gravity += (WaterVertical * MoveAcceleration * Time.deltaTime);
			if(gravity > MaxMoveSpeed * Mathf.Abs(WaterVertical))
				gravity = MaxMoveSpeed * Mathf.Abs(WaterVertical);
			if(gravity < -MaxMoveSpeed * Mathf.Abs(WaterVertical))
				gravity = -MaxMoveSpeed * Mathf.Abs(WaterVertical);
			
			if(gravity > yDist)
				gravity = yDist;
			
			movement.y += gravity * Time.deltaTime;
			return movement;
		}
		else
			IsInWater = false;
		
        if(jumping)
        {
            gravity += GRAVITY * 2f * Time.deltaTime;
            float dist = jumpDestinationY - transform.position.y;
            if (gravity * Time.deltaTime > dist)
                gravity = dist;

            movement.y += gravity * Time.deltaTime;

            if (dist < 0.1f)
                jumping = false;
            else
                return movement;  //End function- still jumping.
        }
        if(jumping == false)
        {
            gravity -= GRAVITY * GravityScale * Time.deltaTime;
            movement.y += gravity * Time.deltaTime;
			if (IsGrounded)
			{
				gravity = 0f;
				movement.y -= 0.1f;
			}
        }
        
            
        

        return movement;
    }

    Vector3 ProcessMoveVec(Vector3 movement)
    {
        if(SuspendMoveSpeedInAir)
        {
            if (!IsGrounded)  //Set to suspend movement in air, and you aren't grounded.  Don't change the course- maintain it.
            {
                movement += (forwardSpeed * 2f * transform.forward * Time.deltaTime) + (rightSpeed * transform.right * Time.deltaTime);
                return movement;
            }
                
        }
        if (MoveVec != Vector2.zero)  //Is moving
        {
            rightSpeed += MoveVec.x * MoveAcceleration * Time.deltaTime;
            if (rightSpeed > MaxMoveSpeed)
                rightSpeed = MaxMoveSpeed;
            if (rightSpeed < -MaxMoveSpeed)
                rightSpeed = -MaxMoveSpeed;


            forwardSpeed += MoveVec.y * MoveAcceleration * Time.deltaTime;
            if (forwardSpeed > MaxMoveSpeed)
                forwardSpeed = MaxMoveSpeed;
            if (forwardSpeed < -MaxMoveSpeed)
                forwardSpeed = -MaxMoveSpeed;
           
        }
        else //Not moving
        {
            rightSpeed = Mathf.MoveTowards(rightSpeed, 0f, SlowDownPerSecond * Time.deltaTime);
            forwardSpeed = Mathf.MoveTowards(forwardSpeed, 0f, SlowDownPerSecond * Time.deltaTime);
        }
        movement += (forwardSpeed * transform.forward * Time.deltaTime) + (rightSpeed * transform.right * Time.deltaTime);
        return movement;
    }

	public void RotateTowardsPoint(Vector3 pt)
	{
		float targetY = Quaternion.LookRotation(pt - transform.position).eulerAngles.y;
        Vector3 currentRot = transform.eulerAngles;
        currentRot.y = Mathf.MoveTowardsAngle(currentRot.y, targetY, rotateSpeed * Time.deltaTime);
        transform.eulerAngles = currentRot;
	}

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.normal.y < 0.75f && hit.normal.y > 0.1f && hit.gameObject.layer != 11)  //Not a floor
        {
			if(IsGrounded == false)
			{
				forwardSpeed = 0f;
				rightSpeed = 0f;
				jumpDestinationY = transform.position.y;
				jumping = false;
			}
        }
		if (hit.normal.y < 0.75f && hit.gameObject.layer == 11)  //Hit boundary
		{
			jumping = false;
		}
  
		hit.collider.gameObject.SendMessage("ControlHit", new ControllerCollisionMessage(gameObject, hit), SendMessageOptions.DontRequireReceiver);
		
		if(hit.rigidbody != null)
		{
			hit.rigidbody.AddForce(hit.normal * hit.moveLength * 50f, ForceMode.Force);
		}
    }
}
