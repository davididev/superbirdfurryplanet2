﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour
{
	public int worldVarID = -1;
	public float worldVarMinRange = 0f;
	public float worldVarMaxRange = 0.2f;
	private bool hover = false;
	
	public TextAsset dialogue;
    // Start is called before the first frame update
    void Start()
    {
        
    }
	
	void OnDestroy()
	{
		if(hover)
		{
			MainUI.IsNPCAvailable = false;
			hover = false;
			MainUI.UpdateUI = true;
		}
	}

    // Update is called once per frame
    void Update()
    {
		if(DialogueHandler.IS_RUNNING)
			return;
       if(worldVarID > -1)
	   {
		   int w = MapVars.WorldID;
		   float v = GameDataHolder.instance.Worlds[w].MissionsProgress[worldVarID];
		   
		   gameObject.SetActive(v >= worldVarMinRange && v <= worldVarMaxRange);
	   }
	   
	   if(hover && Time.timeScale > 0.05f)
	   {
		   if(Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.VolumeUpButton))
		   {
			   DialogueHandler.RunEvent(dialogue);
		   }
	   }
	   
	   
    }
	
	void OnTriggerEnter(Collider c)
	{
		if(c.tag == "Player")
		{
			MainUI.IsNPCAvailable = true;
			hover = true;
			MainUI.UpdateUI = true;
		}
	}
	
	void OnTriggerExit(Collider c)
	{
		if(c.tag == "Player")
		{
			MainUI.IsNPCAvailable = false;
			hover = false;
			MainUI.UpdateUI = true;
		}
	}
}
