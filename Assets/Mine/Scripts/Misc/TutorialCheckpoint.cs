﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialCheckpoint : MonoBehaviour
{
	public int nextID = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	void OnTriggerEnter(Collider col)
	{
		if(col.tag == "Player")
		{
			transform.root.GetComponent<TutorialHandler>().SetCheckpoint(nextID);
			FindObjectOfType<PlayerMovement>().SetCheckpointToCurrentPos();
		}
	}
}
