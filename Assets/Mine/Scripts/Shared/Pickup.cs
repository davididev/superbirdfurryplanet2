﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
	public int amount = 1;
	public enum TYPE {Niara, HP, MP };
	public TYPE powerupType = TYPE.Niara;
	public AudioClip soundFX;
	
	public float fallToBottomInSeconds = 0.25f;
	private float fallingTimer = 0f;
	
    // Start is called before the first frame update
    void OnEnable()
    {

		Invoke("Fall", 0.15f);
    }
	
	void Fall()
	{
		Collider c = GetComponent<Collider>();
		float realBottom = c.bounds.center.y - (c.bounds.size.y / 2f);
		float offset = transform.position.y - realBottom;
		//Debug.Log("Real Bottom: " + realBottom + ".  Offset: " + offset);
		
		RaycastHit info;
        if (Physics.Raycast(transform.position, Vector3.down, out info, 100f, LayerMask.GetMask("Default")))
		{
			Vector3 v = info.point + (Vector3.up * offset);
			iTween.ValueTo(gameObject, iTween.Hash("from", transform.position.y, "to", v.y, "time", fallToBottomInSeconds, "onupdate", "SetY"));
			fallingTimer = fallToBottomInSeconds;
		}
	}
	
	void SetY(float y)
	{
		Vector3 pos = transform.position;
		pos.y = y;
		transform.position = pos;
	}
	
	public void SetExpire()
	{
		StartCoroutine(Expire());
	}
	
	IEnumerator Expire()
	{
		yield return new WaitForSeconds(10f);
		gameObject.SetActive(false);
	}

    // Update is called once per frame
    void Update()
    {
        if(fallingTimer > 0f)
			fallingTimer -= Time.deltaTime;
    }
	
	void OnTriggerEnter(Collider col)
	{
		if(col.tag == "Player")
		{
			if(fallingTimer > 0f)
				iTween.Stop(gameObject);
			if(soundFX != null)
				AudioSource.PlayClipAtPoint(soundFX, Camera.main.transform.position);
			if(powerupType == TYPE.Niara)
			{
				GameDataHolder.instance.Niara += amount;
				if(GameDataHolder.instance.Niara > GameDataHolder.instance.MaxNiara)
					GameDataHolder.instance.Niara = GameDataHolder.instance.MaxNiara;
				
			}
			if(powerupType == TYPE.HP)
			{
				GameDataHolder.instance.MinHP += amount;
				if(GameDataHolder.instance.MinHP > GameDataHolder.instance.MaxHP)
					GameDataHolder.instance.MinHP = GameDataHolder.instance.MaxHP;
				
			}
			if(powerupType == TYPE.MP)
			{
				GameDataHolder.instance.MinMP += amount;
				if(GameDataHolder.instance.MinMP > GameDataHolder.instance.MaxMP)
					GameDataHolder.instance.MinMP = GameDataHolder.instance.MaxMP;
				
			}
			
			MainUI.UpdateUI = true;
			gameObject.SetActive(false);
		}
	}
}
