﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ItemButton : MonoBehaviour
{
	public TMPro.TextMeshProUGUI itemDescpription;
	public int itemNumber = 0;
	
	public float repeatTime = 90f;
	private float pressTimer = 0f, hoverTimer = 0f;
	
	private string itemID;
	private string itemDesc;
	private Image img;
	public ItemDB.SPECIAL spc;
	public InventoryMenu inv;
	
    // Start is called before the first frame update
    public void Refresh()
    {
		Debug.Log("Refreshing item number " + itemNumber);
        itemID = GameDataHolder.instance.items[itemNumber];
		
		itemDesc = ItemDB.GetItemString(itemID);
		img = GetComponent<Image>();
		img.sprite = Resources.Load<Sprite>("Items/" + itemID) as Sprite;
		spc = ItemDB.GetItemSpecial(itemID);
    }
	
	//Message sent from UIPointer.cs
	public void OnHighlight(Vector3 pos)
	{
		hoverTimer = Time.unscaledDeltaTime * 3f;  //Stay hovered for three framesteps

		

		itemDescpription.text = itemDesc;
		img.color = Color.yellow;
	}

    // Update is called once per frame
    void Update()
    {
		if(img == null)
			img = GetComponent<Image>();
		if(pressTimer > 0f)
			pressTimer -= Time.unscaledDeltaTime;
		
        if(hoverTimer > 0f)
		{
			hoverTimer -= Time.unscaledDeltaTime;
			if(hoverTimer <= 0f)
			{
				pressTimer = 0f;
				img.color = Color.white;
				itemDescpription.text = "";
			}
			else
			{
				
				if(pressTimer <= 0f)
				{
					if(Finch.FinchController.Right.GetPress(Finch.FinchControllerElement.Trigger))
					{
						if(spc == ItemDB.SPECIAL.Revive)
							return;
						if(itemID == "0")
							return;
						
						
						if(spc == ItemDB.SPECIAL.Beserk)
							GameDataHolder.instance.BeserkCount = 100;
						
						GameDataHolder.instance.MinHP += ItemDB.GetItemHP(itemID);
						GameDataHolder.instance.MinMP += ItemDB.GetItemMP(itemID);
						
						if(GameDataHolder.instance.MinHP > GameDataHolder.instance.MaxHP)
							GameDataHolder.instance.MinHP = GameDataHolder.instance.MaxHP;
						if(GameDataHolder.instance.MinMP > GameDataHolder.instance.MaxMP)
							GameDataHolder.instance.MinMP = GameDataHolder.instance.MaxMP;
						
						GameDataHolder.instance.items[itemNumber] = "0";
						Refresh();
						
						
						AudioSource.PlayClipAtPoint(inv.useItemSound, Camera.main.transform.position);

					}
				}
			}
		}
		
		
    }
}
