﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
	public AudioClip FireSound;
	public int Bounces = 1;
	public bool UseGravity = false;
	private Vector3 baseVelocity;
	public string ExplosionPrefabName;
	
	public int Element = DmgMessage.ELEMENT_FIRE;
	public int Damage = 1;
	public bool UsePlayerDamage = true;
	
	public float Speed = 10f;
	
	private float gravity = 0f;
	private int currentBounce = 0;
	
	private Rigidbody rigid;
	
	private const float DEATH_TIME = 5f;
	private float deathTimer = 0f;
	
	
	public TrailRenderer trailRend;
    // Start is called before the first frame update
    void OnEnable()
    {
        if(trailRend != null)
			trailRend.Clear();
    }
	
	public void Launch(Vector3 vel)
	{
		deathTimer = 0f;
		baseVelocity = vel * Speed;
		currentBounce = 0;
		gravity = 0f;
		AudioSource.PlayClipAtPoint(FireSound, transform.position);
	}

    // Update is called once per frame
    void FixedUpdate()
    {
		if(rigid == null)
			rigid = GetComponent<Rigidbody>();
		
		deathTimer += Time.fixedDeltaTime;
		if(deathTimer > DEATH_TIME)
			gameObject.SetActive(false);
		
		if(UseGravity)
		{
			gravity += 9.8f * Time.fixedDeltaTime;
			rigid.velocity = baseVelocity + (Vector3.down * gravity);
		}
		else
		{
			rigid.velocity = baseVelocity;
		}
		
    }
	
	void OnCollisionEnter(Collision col)
	{
		Vector3 n = col.contacts[0].normal;
		if(col.gameObject.layer == 0) //Default
		{
			currentBounce++;
			if(currentBounce >= Bounces)
			{
				GameObject g = GameObjectPool.GetInstance(ExplosionPrefabName, transform.position, n);
				gameObject.SetActive(false);
			}
			else
			{
				Vector3 temp = Vector3.Cross(n, Vector3.forward);
				Vector3 v = Vector3.Cross(temp, n);
				
				baseVelocity = ((v + n + baseVelocity).normalized + Vector3.up) * Speed;
				if(UseGravity && baseVelocity.y > (Speed / 2f))
					baseVelocity.y = (Speed / 2f);
				gravity = 0f;
			}
		}
		else  //Might be an enemy!
		{
			int d = Damage;
			if(UsePlayerDamage)
			{
				d = d * GameDataHolder.instance.AttackUpgrades;
			
				if(SwordController.BeserkPhase != SwordController.BeserkMode.NORMAL)
					d = d * 2;
				
				
			}
			col.gameObject.SendMessage("OnDamage", new DmgMessage(d, Element, transform.position, -n), SendMessageOptions.DontRequireReceiver);
			GameObject g = GameObjectPool.GetInstance(ExplosionPrefabName, transform.position, n);
				gameObject.SetActive(false);
		}
	}
}
