﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TargetsHolder : MonoBehaviour
{
	private float timeElapsed = 0f;
	private float restoreTimer = 0f;
	
	public UnityEvent onDone;
	
    // Start is called before the first frame update
    void OnEnable()
    {
        foreach(Transform t in transform)
		{
			t.gameObject.SetActive(true);
		}
    }

    // Update is called once per frame
    void Update()
    {
		restoreTimer += Time.deltaTime;
		if(restoreTimer > 0.15f)
		{
			GameDataHolder.instance.MinMP += 1;
			if(GameDataHolder.instance.MinMP > GameDataHolder.instance.MaxMP)
				GameDataHolder.instance.MinMP = GameDataHolder.instance.MaxMP;
			restoreTimer = 0f;
		}
        timeElapsed += Time.deltaTime;
		int countRemaining = 0;
		foreach(Transform t in transform)
		{
			if(t.gameObject.activeSelf == true)
				countRemaining++;
		}
		
		if(countRemaining == 0)
		{
			DialogueHandler.SetVar("%time", Mathf.Floor(timeElapsed));
			onDone.Invoke();
			gameObject.SetActive(false);
		}
    }
}
