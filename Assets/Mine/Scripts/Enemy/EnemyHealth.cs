﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
	public MeshRenderer rendFallback;
	public SkinnedMeshRenderer rend;
	
	public int MaxHealth = 3;
	private int MinHealth;
	
	public string deathPrefabName = "DeathPrefab";
	
	public int reactionToSword = 100, reactionToFire = 100, reactionToIce = 100, reactionToShadow = 100, reactionToLightning = 100, reactionToCosmic = 100;
	
	private const float FLASH_TIME = 0.25f;
	private float flashTimer = 0f;
	
	[System.Serializable]
	public struct Pickup
	{
		public int chance;
		public int id;
		public int count;
	}
	
	public Pickup[] pickupsOnDeath;
	
	
    // Start is called before the first frame update
    void OnEnable()
    {
        MinHealth = MaxHealth;
		if(rendFallback != null)
			rendFallback.material.SetColor("_EmissionColor", Color.black);
		if(rend != null)
			rend.material.SetColor("_EmissionColor", Color.black);
    }
	
	public void OnDamage(DmgMessage m)
	{
		GameDataHolder.instance.BeserkCount++;
		int d = m.amount;
		if(m.element == DmgMessage.ELEMENT_NONE)
			d = d * reactionToSword / 100;
		if(m.element == DmgMessage.ELEMENT_FIRE)
			d = d * reactionToFire / 100;
		if(m.element == DmgMessage.ELEMENT_ICE)
			d = d * reactionToIce / 100;
		if(m.element == DmgMessage.ELEMENT_SHADOW)
			d = d * reactionToShadow / 100;
		if(m.element == DmgMessage.ELEMENT_LIGHTNING)
			d = d * reactionToLightning / 100;
		if(m.element == DmgMessage.ELEMENT_COSMIC)
			d = d * reactionToCosmic / 100;
		
		
		GameObject dmg = GameObjectPool.GetInstance("DMGStar", m.hitPoint, Vector3.zero);
		if(dmg != null)
			dmg.SendMessage("Display", d);
		
		Color c1 = new Color(0f, 0f, 0f, 1f);
		Color c2 = new Color(0.5f, 0f, 0f, 1f);
		Color c3 = new Color(0f, 0.5f, 0f, 1f);
		
		MinHealth -= d;
		if(d > 0 && flashTimer <= 0f && MinHealth > 0)
		{
			flashTimer = FLASH_TIME;
			float t = FLASH_TIME / 2f;
			iTween.ValueTo(gameObject, iTween.Hash("from", c1, "to", c2, "onupdate", "SetEmissive", "time", t));
			iTween.ValueTo(gameObject, iTween.Hash("from", c2, "to", c1, "onupdate", "SetEmissive", "time", t, "delay", t));
			
			if(ccm == null)
				ccm = GetComponent<CharacterControllerMovement>();
			
			ccm.AddForce(4f * m.normal, 2f);
			
		}
		if(d < 0 && flashTimer <= 0f)
		{
			//Less than 0- heal him.
			flashTimer = FLASH_TIME;
			float t = FLASH_TIME / 2f;
			iTween.ValueTo(gameObject, iTween.Hash("from", c1, "to", c3, "onupdate", "SetEmissive", "time", t));
			iTween.ValueTo(gameObject, iTween.Hash("from", c3, "to", c1, "onupdate", "SetEmissive", "time", t, "delay", t));
			
			if(MinHealth > MaxHealth)
				MinHealth = MaxHealth;
		}
		
		float p = (MinHealth * 100 / MaxHealth) / 100f;
		MainUI.SetEnemyPerc(p);
		
		
		
		if(MinHealth <= 0)
			Kill();
		
		
		
		GameDataHolder.instance.BeserkCount++;
		if(GameDataHolder.instance.SelectedRelic == 3)
			GameDataHolder.instance.BeserkCount++;
	}
	
	CharacterControllerMovement ccm;
	
	public void Kill()
	{
		GameObject dust = GameObjectPool.GetInstance(deathPrefabName, transform.position, transform.rotation);
		int c = Random.Range(1, 100);
		int id = -1;
		while(c > 0)
		{
			id++;
			c -= pickupsOnDeath[id].chance;
		}
		for(int i = 0; i < pickupsOnDeath[id].count; i++)
		{
			Vector3 v = transform.position;
			v.x += Random.Range(-1f, 1f);
			v.z += Random.Range(-1f, 1f);
			GameObject g = GameObjectPool.GetInstance("Spoil" + pickupsOnDeath[id].id, v, Vector3.zero);
			if(g != null)
				g.SendMessage("SetExpire");
		}
		
		//GameObject g2 = GameObjectPool.GetInstance("DeathPrefab", transform.position, Vector3.zero);
		gameObject.SetActive(false);
	}
	
	private void SetEmissive(Color c)
	{
		
		if(rend != null)
            rend.material.SetColor("_EmissionColor", c);
		if(rendFallback != null)
            rendFallback.material.SetColor("_EmissionColor", c);
	}

    // Update is called once per frame
    void Update()
    {
        if(flashTimer > 0f)
		{
			flashTimer -= Time.deltaTime;
		}
    }
	
	
}
