﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatUpgrade : MonoBehaviour
{
	public enum TYPE { HealthUpgrade=0, MagicUpgrade=1, FeatherUpgrade=2, Attack=3, Nil=4};
	public TYPE upgradeType = TYPE.HealthUpgrade;
	public AudioClip soundFXOnUpgrade;
	
    // Start is called before the first frame update
    void Start()
    {
		StartCoroutine(SetIsEnabled());
    }
	
	IEnumerator SetIsEnabled()
	{
		yield return new WaitForSecondsRealtime(0.05f);
		while(UIPointer.IsActive == false)
		{
			yield return new WaitForSecondsRealtime(0.1f);
		}
		GameData.WorldInfo info = GameDataHolder.instance.Worlds[MapVars.WorldID];
		
		char[] cs = info.Powerups.ToCharArray();
		int i = (int) upgradeType;
		if(cs[i] == '1')
			gameObject.SetActive(false);
	}

    // Update is called once per frame
    void Update()
    {
        
    }
	
	void OnTriggerEnter(Collider c)
	{
		if(c.tag == "Player")
		{
			GameData.WorldInfo info = GameDataHolder.instance.Worlds[MapVars.WorldID];
		
			char[] cs = info.Powerups.ToCharArray();
			int i = (int) upgradeType;
			cs[i] = '1';
			info.Powerups = new string(cs);
			
			if(upgradeType == TYPE.HealthUpgrade)
				GameDataHolder.instance.HPUpgrades++;
			if(upgradeType == TYPE.MagicUpgrade)
				GameDataHolder.instance.MPUpgrades++;
			if(upgradeType == TYPE.FeatherUpgrade)
				GameDataHolder.instance.FeatherUpgrades++;
			if(upgradeType == TYPE.Attack)
				GameDataHolder.instance.AttackUpgrades++;
			
			MainUI.UpdateUI = true;
			if(soundFXOnUpgrade != null)
				AudioSource.PlayClipAtPoint(soundFXOnUpgrade, Camera.main.transform.position);
			gameObject.SetActive(false);
		}
	}
}
