﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionsMenu : MonoBehaviour
{
	protected int currentWorld = 0;
	
	public TMPro.TextMeshProUGUI worldName;
	public TMPro.TextMeshProUGUI[] missionNames;
	public Image[] missionProgress;
	public GameObject[] missionCheck;
	
	
    // Start is called before the first frame update
    void Start()
    {
        
    }
	
	public void Refresh()
	{
		if(MapVars.WorldID != -1)
			currentWorld = MapVars.WorldID;
		DrawCurrentWorld();

	}
	
	public void UpWorld()
	{
		currentWorld++;
		if(currentWorld > 7)
			currentWorld = 0;
		DrawCurrentWorld();
	}
	
	public void DownWorld()
	{
		currentWorld++;
		if(currentWorld < 0)
			currentWorld = 7;
		DrawCurrentWorld();
	}
	
	void DrawCurrentWorld()
	{
		MissionsDB.World w = MissionsDB.Instance.worlds[currentWorld];
		
		worldName.text = (currentWorld+1) + ": " + w.worldName;
		if(currentWorld == MapVars.WorldID)
		{
			worldName.text = worldName.text + " (current)";
			worldName.color = Color.yellow;
		}
		else
			worldName.color = Color.white;
		
		
		for(int i = 0; i < 10; i++)
		{
			missionNames[i].text = w.missionTexts[i];
			float perc = GameDataHolder.instance.Worlds[currentWorld].MissionsProgress[i];
			missionProgress[i].color = Color.Lerp(Color.white, Color.green, perc);
			missionProgress[i].fillAmount = perc;
			
			missionCheck[i].SetActive(Mathf.Approximately(perc, 1f));
		}
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
