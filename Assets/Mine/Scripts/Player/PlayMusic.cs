﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayMusic : MonoBehaviour
{
    AudioSource mainSrc;
    private string lastSong = "";
    private Coroutine routine;

    private bool paused = false;
	private float musicPosition = 0f;
	
	private bool firstSong = true;
	
	public static bool UpdateVolume = true;
    // Start is called before the first frame update
    void Start()
    {
        mainSrc = gameObject.AddComponent<AudioSource>();
        mainSrc.minDistance = 5f;
        mainSrc.ignoreListenerVolume = true;
    }

    public void PlaySong(AudioClip loop, AudioClip intro = null)
    {
		try
		{
        if(lastSong != loop.name)
        {
            if (firstSong == false)
                StopCoroutine(routine);
			firstSong = false;
			lastSong = loop.name;
			if(intro == null)
				routine = StartCoroutine(MusicRoutine(loop));
			else
				routine = StartCoroutine(MusicRoutine(loop, intro));
        }
		}
		catch(System.Exception e)
		{
			Debug.Log("Error on PlaySong: " + e.ToString());
		}
    }

    IEnumerator MusicRoutine(AudioClip loop, AudioClip intro = null)
    {
        if(intro != null)
        {
			try
			{
            mainSrc.clip = intro;
            mainSrc.loop = false;
            mainSrc.Play();
			}
			catch(System.Exception e)
			{
				Debug.Log("Error playing intro clip: " + e.ToString());
			}
            while(mainSrc.isPlaying)
            {
                yield return new WaitForSecondsRealtime(0.1f);
            }
        }

		try
		{
		musicPosition = 0f;
        mainSrc.clip = loop;
        mainSrc.loop = true;
        mainSrc.Play();
		}
		catch(System.Exception e)
		{
			Debug.Log("Error playing main src: " + e.ToString());
		}
		yield return null;
    }

    // Update is called once per frame
    void Update()
    {
		
        if(Time.timeScale < 0.05f)
        {
            if(paused == false)
            {
                paused = true;
				if(mainSrc != null)
				{
					musicPosition = mainSrc.time;
					mainSrc.volume = 0f;
					//mainSrc.Stop();
				}
            }
        }
        else
        {
            if (paused == true)
            {
			if(UpdateVolume == true)
			{
				UpdateVolume = false;
				AudioListener.volume = GameDataHolder.instance.VolumeSound;
			}
                paused = false;
				if(mainSrc != null)
				{
					mainSrc.volume = GameDataHolder.instance.VolumeMusic;
					mainSrc.time = musicPosition;
				}
            }
        }
    }
}
