﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    private Animator anim;
    public AudioClip singleJumpFX, doubleJumpFX, tripleJumpFX, squackFX, swimFX;
	public static Vector3 Position, HeadPosition;
    public const float TRIPLE_JUMP_DELAY = 0.5f, SWIM_FRC = 15f, UNDERWATER_TIME = 40f;
    private CharacterControllerMovement ccm;
    public HeadRotator head;
    public GameObject DustPrefab;
	public PauseMenu pauseMenu;
	protected Vector3 checkpointPos;
	private float airTimer = 40f;
	


	public ScreenshotHandler screnHandle;
    private bool retractCamera = false;

    public Transform neckBone, eyePosition, topOfHeadPosition;
	
	public static bool IsRunning = false;
	public static bool IsFrozen = false;
	
	public GameObject[] spoilPrefabs, bulletPrefabs, bulletExplosionPrefabs;
	public GameObject enemyDeathPrefab, dmgStarPrefab, splashPrefab;
	
	private bool firstRun = true;
	
    // Start is called before the first frame update
    void OnEnable()
    {
		checkpointPos = transform.position;
        SceneManager.activeSceneChanged += OnSceneChange;
		if(firstRun)
			OnSceneChange(SceneManager.GetActiveScene(), SceneManager.GetActiveScene());
    }
	
	void OnSceneChange(Scene s1, Scene s2)
	{
		Invoke("InitPool", 0.05f);
		
		firstRun = false;
	}
	
	void InitPool()
	{
		checkpointPos = transform.position;
		GameObjectPool.InitPoolItem("Splash", splashPrefab, 30);
		GameObjectPool.InitPoolItem("DMGStar", dmgStarPrefab, 30);
		GameObjectPool.InitPoolItem("PlayerDust", DustPrefab, 40);
		GameObjectPool.InitPoolItem("DeathPrefab", enemyDeathPrefab, 15);
		for(int i = 0; i < spoilPrefabs.Length; i++)
		{
			GameObjectPool.InitPoolItem("Spoil" + i, spoilPrefabs[i], 15);
		}
		
		for(int i = 0; i < bulletPrefabs.Length; i++)
		{
			GameObjectPool.InitPoolItem("Bullet" + (i+1), bulletPrefabs[i], 15);
			GameObjectPool.InitPoolItem("BulletEXP" + (i+1), bulletExplosionPrefabs[i], 15);
		}
		
		if(SceneManager.GetActiveScene().name == "GoldenRuler_Inside")
		{
			GameDataHolder.instance.MinHP = GameDataHolder.instance.MaxHP;
			GameDataHolder.instance.MinMP = GameDataHolder.instance.MaxMP;
			FindObjectOfType<SwordController>().SetElement(0);
		}
	}
	
	void OnDisable()
	{
		SceneManager.activeSceneChanged -= OnSceneChange;
	}
	
    private float tripleJumpTimer = 0f;
	private float featherRestoreTimer = 0f;
    private int tripleJumpCounter = 0;

	
	private bool makeGroundDust = false;
	float pauseHeldTime = 0f;
    float gravityResetTimer = 0f, makeGroundDustTimer = 9000000f;
	float cc_radius = 0f;
	
	private bool isFlying = false;
	private float swimTimer = 0f, dustTimer = 0f;
	
	
	private bool firstWaterDive = true;
	
	void EnableCCM()
	{
		ccm.enabled = true;
	}
	
	public void SetCheckpointToCurrentPos()
	{
		checkpointPos = transform.position;
	}
	
    // Update is called once per frame
    void Update()
    {
        if (ccm == null)
            ccm = GetComponent<CharacterControllerMovement>();
        if (anim == null)
            anim = GetComponent<Animator>();
		if(IsFrozen)
		{
			ccm.MoveVec = Vector2.zero;
			ccm.ClearMoveVec();
			return;
		}
		if(DialogueHandler.IS_RUNNING)
		{
			ccm.MoveVec = Vector2.zero;
			return;
		}
		IsRunning = (ccm.Velocity.y >= 7.5f);

		Position = transform.position;
		
		if(Mathf.Approximately(cc_radius, 0f))
			cc_radius = ccm.cc.radius;

		if(transform.position.y < MapVars.DeathY)
		{
			//Damage later
			gameObject.SendMessage("OnDamage", new DmgMessage(3, 0, Vector3.zero, Vector3.zero));
			MainUI.UpdateUI = true;
			ccm.enabled = false;
			transform.position = checkpointPos;
			Invoke("EnableCCM", Time.deltaTime);
		}
        Vector3 playerRot = transform.eulerAngles;
        playerRot.y = HeadRotator.RealFacing.y;
        transform.eulerAngles = playerRot;

        anim.SetFloat("RunningMultiplier", Mathf.Abs(ccm.Velocity.y / ccm.MaxMoveSpeed));
        anim.SetBool("IsGrounded", ccm.IsGrounded);
		anim.SetBool("IsSwimming", ccm.IsInWater);

        Vector2 touch = Finch.FinchController.Right.TouchAxes;
        ccm.MoveVec = Vector2.zero;
        if (touch.y > 0.33f)
            ccm.MoveVec = new Vector2(0f, 1f);
        if (touch.y < -0.33f)
            ccm.MoveVec = new Vector2(0f, -1f);

		if(ccm.IsInWater)
		{
			if(firstWaterDive == true)
			{
				airTimer = UNDERWATER_TIME;
				firstWaterDive = false;
			}
			if(ccm.IsUnderWater)
			{
				firstWaterDive = false;
				airTimer -= Time.deltaTime;
				if(airTimer < 0f)
				{
					airTimer = 0f;
					gameObject.SendMessage("OnDamage", new DmgMessage(3, 0, Vector3.zero, Vector3.zero));
				}
			}
			if(!ccm.IsUnderWater)
				firstWaterDive = true;
			
			MainUI.AirPerc = airTimer / UNDERWATER_TIME;
			MainUI.UpdateUI = true;
			
			
			ccm.WaterVertical = 0.15f;
			ccm.ClearMoveVec();
			if(Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.ThumbButton) && swimTimer <= 0f)
			{
				anim.SetTrigger("Jump");
				swimTimer = 0.5f;
				ccm.AddForce(SWIM_FRC * Camera.main.transform.forward, 2f);
				AudioSource.PlayClipAtPoint(swimFX, transform.position);
			}
			if(swimTimer > 0f)
				swimTimer -= Time.deltaTime;
		}
		else
		{
			firstWaterDive = true;
			MainUI.AirPerc = -1f;
			MainUI.UpdateUI = true;
			
			if(ccm.IsGrounded)
			{
				tripleJumpTimer += Time.deltaTime;
				//Process triple jump counter
				if(gravityResetTimer <= 0f)
					ccm.GravityScale = 1f;
				if(makeGroundDustTimer >= 0f)
				{
					makeGroundDustTimer -= Time.deltaTime;
				}
				featherRestoreTimer += Time.deltaTime;
				float max = 5f;
				if(GameDataHolder.instance.SelectedRelic == 1)
					max = 1f;
			
				if(featherRestoreTimer >= max)
				{
					GameDataHolder.instance.MinFeathers++;
					if(GameDataHolder.instance.MinFeathers > GameDataHolder.instance.MaxFeathers)
						GameDataHolder.instance.MinFeathers = GameDataHolder.instance.MaxFeathers;
					featherRestoreTimer = 0f;
					MainUI.UpdateUI = true;
				}
				
			}	
			if (Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.ThumbButton))
			{
			

			if (gravityResetTimer > 0f)
				gravityResetTimer -= Time.deltaTime;
			if (ccm.IsGrounded)
			{
				
				
				if(makeGroundDustTimer <= 0f)
				{
					makeGroundDustTimer = 9000000f;
					makeGroundDust = false;
					//Spawn dust particle here.
					GameObject walkBlockParticle = GameObjectPool.GetInstance("PlayerDust", transform.position + Vector3.down, Quaternion.identity);
					walkBlockParticle.transform.up = Vector3.up;
				}
				
				firstWallPlay = true;
			
				//Jump
            
            
                
                Debug.Log("Jump vars: " + tripleJumpCounter + "; " + tripleJumpTimer);
                if (tripleJumpTimer > TRIPLE_JUMP_DELAY)
                    tripleJumpCounter = 0;

                tripleJumpCounter++;
                tripleJumpTimer = 0f;
                anim.SetTrigger("Jump");
				
				if (tripleJumpCounter >= 3 && ccm.Velocity.y < 7.5f)
					tripleJumpCounter = 1;
				
                if(tripleJumpCounter == 1)
                {
					ccm.GravityScale = 1f;
                    ccm.Jump(4f);
                    AudioSource.PlayClipAtPoint(singleJumpFX, transform.position);
                }
                if (tripleJumpCounter == 2)
                {
					ccm.GravityScale = 1f;
                    ccm.Jump(10f);
                    AudioSource.PlayClipAtPoint(doubleJumpFX, transform.position);
                }
				if (tripleJumpCounter >= 3 && !IsRunning)
				{
					ccm.GravityScale = 1f;
					ccm.Jump(10f);
                    AudioSource.PlayClipAtPoint(doubleJumpFX, transform.position);
				}
                if (tripleJumpCounter >= 3 && IsRunning)
                {
                    ccm.Jump(14f);
                    gravityResetTimer = 0.2f;
                    ccm.GravityScale = 0.05f;
                    AudioSource.PlayClipAtPoint(tripleJumpFX, transform.position);
                }
				 
				
				makeGroundDustTimer = 0.2f;

                if (touch.x > 0.33f)
                    ccm.AddForce(transform.right * 12f, 1f);
                if (touch.x < -0.33f)
                    ccm.AddForce(transform.right * -12f, 1f);
            
            
			}
			}
			if(ccm.IsGrounded == false)
			{
			    //Jump off walls (maybe)
					Vector3 dir = Vector3.zero;
					RaycastHit info;
					if (Physics.Raycast(ccm.Center, transform.forward, out info, cc_radius + 1f, LayerMask.GetMask("Default")))
						dir = info.normal;
					if (Physics.Raycast(ccm.Center, -transform.forward, out info, cc_radius + 1f, LayerMask.GetMask("Default")))
                        dir = info.normal;
					if (Physics.Raycast(ccm.Center, transform.right, out info, cc_radius + 1f, LayerMask.GetMask("Default")))
                        dir = info.normal;
					if (Physics.Raycast(ccm.Center, -transform.right, out info, cc_radius + 1f, LayerMask.GetMask("Default")))
                        dir = info.normal;

				if (dir != Vector3.zero)
				{	
					
					if(dustTimer <= 0f)
					{
						GameObject walkBlockParticle = GameObjectPool.GetInstance("PlayerDust", transform.position - (dir * cc_radius), Quaternion.identity);
						firstWallPlay = false;
						walkBlockParticle.transform.up = dir;
						dustTimer = 0.1f;
					}
					else
						dustTimer -= Time.deltaTime;
						
                    
					if (Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.ThumbButton))
					{
						ccm.ClearMoveVec();
						ccm.AddForce(dir * 30f, 1f);
						ccm.Jump(6f);
						AudioSource.PlayClipAtPoint(squackFX, transform.position);
					}
				}
			}
				if (Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.ThumbButton))
				{
				//Flying
				if(isFlying)
				{
					if(GameDataHolder.instance.MinFeathers > 0)
					{
						anim.SetTrigger("Jump");
						ccm.Jump(8f);
						AudioSource.PlayClipAtPoint(singleJumpFX, transform.position);
						GameDataHolder.instance.MinFeathers--;
						MainUI.UpdateUI = true;
					}
				}
            
			}
			}
		
		//Pause/take screenshot
		if (Finch.FinchController.Right.GetPress(Finch.FinchControllerElement.AppButton))
            pauseHeldTime += Time.deltaTime;
		
		if(Time.timeScale < 0.05f)
        {
                //Is paused
            if (Finch.FinchController.Right.GetPressUp(Finch.FinchControllerElement.AppButton))
			{
				pauseMenu.Close();
			}
        }
        else
        {
			if (Finch.FinchController.Right.GetPressUp(Finch.FinchControllerElement.AppButton))
			{
				if(pauseHeldTime >= 0.25f)  //Screenshot
				{
					//screenhandler.SaveScreenshot();
					Debug.Log("Screenshot.");
					screnHandle.SaveScreenshot();
				}
				else
				{
					pauseMenu.gameObject.SetActive(true);
				}
				pauseHeldTime = 0f;
			}
			
		}		
		if(Time.timeScale > 0.05f)
		{
			//Test
			///if(Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.VolumeUpButton))
			//{
				//GameDataHolder.instance.ElementsUnlocked = "1000";
				//ElementalCube.ResetMat = true;
			//}
        }
		//Retract camera
        if(Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.VolumeDownButton))
        {
            retractCamera = !retractCamera;
        }
        
		
        
        

    }
	
	public void StartFlying()
	{
		gravityResetTimer = 0.5f;
		ccm.GravityScale = 0.05f;
		ccm.Jump(20f);
		isFlying = true;
		anim.SetTrigger("Jump");
	}
	
    bool firstWallPlay = true;
    private void LateUpdate()
    {
        Vector3 rot = neckBone.localEulerAngles;
        rot.x = HeadRotator.RealFacing.x;
        rot.y = HeadRotator.RealFacing.z;
        neckBone.localEulerAngles = rot;

		HeadPosition = topOfHeadPosition.position;

        //Run camera
        if (retractCamera == false)
        {
            head.transform.position = eyePosition.position;
        }
        else
        {
            float distance = 8f;
            RaycastHit info;
            if (Physics.Raycast(transform.position, -Camera.main.transform.forward, out info, distance, LayerMask.GetMask("Default")))
            {
                distance = info.distance - 1f;
            }

            head.transform.position = transform.position + (-Camera.main.transform.forward * distance) + (Vector3.up * 1.5f);
        }
    }
	
	private void OnControllerColliderHit(ControllerColliderHit hit)
	{
		//Layer 11 is Player Boundary
		if(isFlying == true && hit.gameObject.layer != 11)
		{
			ccm.GravityScale = 1f;
			isFlying = false;
			AudioSource.PlayClipAtPoint(squackFX, transform.position);
		}
	}
}
