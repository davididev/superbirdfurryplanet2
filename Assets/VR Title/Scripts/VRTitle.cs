﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRTitle : MonoBehaviour
{
    public GameObject newFile, selectFile, fileSelectedPanel;
    public TextAsset creditsFile;
    public TMPro.TextMeshProUGUI creditsTxt;

    // Start is called before the first frame update
    void Start()
    {
        BoilerRoot.DestroyMe();
        Time.timeScale = 1f;
        
        StartCoroutine(CreditsRoutine());
        StartCoroutine(SettingPanel());
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    IEnumerator SettingPanel()
    {
        //Don't show any panels until the brush is active
        yield return new WaitForEndOfFrame();
        SetPanelID(-1);
        while (TitleBrush.isWorking == false)
        {
            yield return new WaitForEndOfFrame();
        }
        SetPanelID(0);
    }

    public void SetPanelID(int id)
    {
        selectFile.SetActive(id == 0);
        newFile.SetActive(id == 1);
        fileSelectedPanel.SetActive(id == 2);
    }

    IEnumerator CreditsRoutine()
    {
        string[] lines = creditsFile.text.Split('*');
		creditsTxt.text = lines[0];
		int i = 0;
        while (gameObject)
        {
			float angleY = HeadRotator.RealFacing.y;
			
			if(Mathf.Abs(Mathf.DeltaAngle(angleY, 180f)) < 45f)
			{
				creditsTxt.color = Color.white;
                creditsTxt.text = lines[i];
                i++;
				if(i > lines.Length)
					i = 0;
			}
			else
				creditsTxt.color = Color.grey;
			yield return new WaitForSecondsRealtime(10f);
        }
}

    // Update is called once per frame
    void Update()
    {
        
    }
}
