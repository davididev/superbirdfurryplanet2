﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetBullsEye : MonoBehaviour
{
    // Start is called before the first frame update
    void OnEnable()
    {
        transform.localScale = Vector3.one;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	IEnumerator Dmg()
	{
		float t = 0.2f;
		iTween.ScaleTo(gameObject, Vector3.zero, t);
		yield return new WaitForSeconds(t);
		gameObject.SetActive(false);
	}
	
	public void OnDamage(DmgMessage m)
	{
		StartCoroutine(Dmg());
	}
}
