﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordController : MonoBehaviour
{
	public const int ELEMENT_TELEKINESIS = 0, ELEMENT_FIRE = 1, ELEMENT_ICE = 2, ELEMENT_DARK = 3, ELEMENT_LIGHTNING = 4;
	public static readonly int[] MP_COSTS = {1, 1, 2, 4, 6};
	public enum LaunchMode { NotLaunched, ReachOut, ReachIn };
	public const float TELE_SPEED = 25f, MAX_TELE_DISTANCE = 50f;
	
	protected LaunchMode currentMode = LaunchMode.NotLaunched;
	public int elementID = 0;
	private Transform swordChild;
	public Transform hitPoint;
	public MeshRenderer rend;
	
	private const float MIN_SWING_SPEED = 10f;
	
	public AudioClip launchSword, swingSword, changeElementFX;
	public TrailRenderer trailRend;
	public ParticleSystem[] elementChangedParticle;
	
	private bool firstSwing = true;
	
	private float mpRestoreTimer = 0f;
	
	public enum BeserkMode { NORMAL, TEMP, PERM};
	public static BeserkMode BeserkPhase = BeserkMode.NORMAL;
	
	
	
	public Animator beserkAnim;
	
    // Start is called before the first frame update
    void Start()
    {
        //elementID = ELEMENT_FIRE;
    }
	
	private void SetEmissive(Color c)
	{
		
		if(rend != null)
            rend.material.SetColor("_EmissionColor", c);
	}
	
	public void SetElement(int id)
	{
		AudioSource.PlayClipAtPoint(changeElementFX, transform.position);
		elementID = id;
		Color c1 = Color.black;
		Color c2 = Color.white;
			if(elementID == ELEMENT_FIRE)
				c2 = new Color(1f, 0.5f, 0f, 1f);
			if(elementID == ELEMENT_ICE)
				c2 = new Color(1f, 0f, 1f, 1f);
			if(elementID == ELEMENT_DARK)
				c2 = new Color(1f, 0f, 1f, 1f);
			if(elementID == ELEMENT_LIGHTNING)
				c2 = new Color(1f, 1f, 0f, 1f);
			float t = 3f;
		iTween.ValueTo(gameObject, iTween.Hash("from", c1, "to", c2, "onupdate", "SetEmissive", "time", t));
		iTween.ValueTo(gameObject, iTween.Hash("from", c2, "to", c1, "onupdate", "SetEmissive", "time", t, "delay", t));
		
		foreach(ParticleSystem p in elementChangedParticle)
		{
			p.startColor = c2;
			p.Play();
		}
	}

	private float beserkTimer = 0f, lastBeserkTimer = 0f;
    // Update is called once per frame
    void Update()
    {
		if(DialogueHandler.IS_RUNNING)
		{
			trailRend.emitting = false;
			return;
		}
		
		if(GameDataHolder.instance.BeserkCount >= 100)
		{
			if(BeserkPhase == BeserkMode.NORMAL)
			{
				Debug.Log("BESERK TIME");
				GameDataHolder.instance.BeserkCount = 0;
				BeserkPhase = BeserkMode.TEMP;
				beserkTimer = 20f;
				lastBeserkTimer = beserkTimer;
				beserkAnim.SetTrigger("Temp");
			}
		}
		if(beserkTimer > 0f)
		{
			beserkTimer -= Time.deltaTime;
			
			if(Mathf.FloorToInt(beserkTimer) != Mathf.FloorToInt(lastBeserkTimer))
			{
				GameDataHolder.instance.MinMP++;
				if(GameDataHolder.instance.MinMP > GameDataHolder.instance.MaxMP)
					GameDataHolder.instance.MinMP = GameDataHolder.instance.MaxMP;
			}
			lastBeserkTimer = beserkTimer;
			
			if(beserkTimer <= 0f)
				BeserkPhase = BeserkMode.NORMAL;
		}
        if(Time.timeScale > 0.05f)  //Don't move while paused
		{
			transform.eulerAngles = UIPointer.HandRotation;
			CastSpells();
			RunTelekinesis();
			RunTrailRend();
			
			mpRestoreTimer += Time.deltaTime;
			float m = 10f;  //By default, every 10 seconds.
			if(GameDataHolder.instance.SelectedRelic == 0)
				m = 5f;  //Every 5 seconds if Relic 1 is turned on.
			if(mpRestoreTimer >= m)  
			{
				int mp = GameDataHolder.instance.MinMP;
				mp += 1;
				if(mp > GameDataHolder.instance.MaxMP)
					mp = GameDataHolder.instance.MaxMP;
			
				GameDataHolder.instance.MinMP = mp;
				mpRestoreTimer = 0f;
				MainUI.UpdateUI = true;
			}
		}
    }
	
	bool UseMP(int spellID)
	{
		int mp = GameDataHolder.instance.MinMP;
		if(mp >= MP_COSTS[spellID])
		{
			mp -= MP_COSTS[spellID];
			GameDataHolder.instance.MinMP = mp;
			MainUI.UpdateUI = true;
			return true;
		}
		
		return false;
	}
	
	void CastSpells()
	{
		if(Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.Trigger))
		{
			if(elementID == ELEMENT_TELEKINESIS)
			{
				if(currentMode == LaunchMode.NotLaunched)
				{
					if(UseMP(ELEMENT_TELEKINESIS))
					{
						AudioSource.PlayClipAtPoint(launchSword, transform.position);
						currentMode = LaunchMode.ReachOut;
					}
				}
			}
			else
			{
				if(UseMP(elementID))
				{
					GameObject bullet = GameObjectPool.GetInstance("Bullet" + elementID, hitPoint.position, hitPoint.eulerAngles);
					bullet.SendMessage("Launch", hitPoint.forward);
				}
			}
		}
	}
	
	void RunTelekinesis()
	{
		if(swordChild == null)
			swordChild = transform.GetChild(0);
		
		if(currentMode == LaunchMode.ReachOut)
		{
			Vector3 v = swordChild.localPosition;
			v.z += TELE_SPEED * Time.deltaTime;
			swordChild.localPosition = v;
			
			if(v.z > MAX_TELE_DISTANCE)
				currentMode = LaunchMode.ReachIn;
		}
		if(currentMode == LaunchMode.ReachIn)
		{
			Vector3 v = swordChild.localPosition;
			v.z = Mathf.MoveTowards(v.z, 1f, TELE_SPEED * 2f * Time.deltaTime);
			swordChild.localPosition = v;
			
			if(Mathf.Approximately(v.z, 1f))
				currentMode = LaunchMode.NotLaunched;
		}
	}
	
	void RunTrailRend()
	{
		if(currentMode == LaunchMode.NotLaunched)
		{
			float swingForce = Finch.FinchController.Right.AngularVelocity.sqrMagnitude;
			trailRend.emitting = (swingForce > MIN_SWING_SPEED);
			if(swingForce < MIN_SWING_SPEED)
			{
				//trailRend.Clear();
				firstSwing = true;
			}
			else
			{
				if(firstSwing)
				{
					firstSwing = false;
					AudioSource.PlayClipAtPoint(swingSword, transform.position);
				}
			}
		}
		else
			trailRend.emitting = true;
		
	}
	
	void OnTriggerEnter(Collider c)
	{
		if(c.gameObject.tag == "Player")
			return;
		bool hitSuccess = true;
		if(currentMode == LaunchMode.NotLaunched)
		{
			float swingForce = Finch.FinchController.Right.AngularVelocity.sqrMagnitude;
			//Debug.Log("Swing Force: " + swingForce);
			
			if(swingForce < MIN_SWING_SPEED)
				hitSuccess = false;
		}
		if(currentMode == LaunchMode.ReachOut)
		{
			currentMode = LaunchMode.ReachIn;
		}
		
		if(hitSuccess)
		{
			
			int dmg = GameDataHolder.instance.AttackUpgrades * 2;
			if(BeserkPhase != BeserkMode.NORMAL)
				dmg = dmg * 2;
			c.gameObject.SendMessage("OnDamage", new DmgMessage(dmg, 0, hitPoint.position, hitPoint.forward), SendMessageOptions.DontRequireReceiver);
		}
	}
}
