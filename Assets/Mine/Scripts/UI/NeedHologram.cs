﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeedHologram : MonoBehaviour
{
	public TMPro.TextMeshProUGUI txt;
    // Start is called before the first frame update
    void Start()
    {
        
    }
	
	public void SetTxt(string s)
	{
		txt.text = s;
	}

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(Camera.main.transform.position);
    }
}
