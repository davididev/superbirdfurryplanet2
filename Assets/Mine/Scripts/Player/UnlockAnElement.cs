﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockAnElement : MonoBehaviour
{
	public AudioClip playOnUnlock;
	public TextAsset dialogueOnFinished;
	public int elementID = 0;  //Fire
	public ParticleSystem emitOnUnlocked;
    // Start is called before the first frame update
    void Start()
    {
        char c = GameDataHolder.instance.ElementsUnlocked[elementID];
		if(c == '1')
			gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	bool isPlaying = false;
	void OnTriggerEnter(Collider col)
	{
		if(col.tag == "Player" && isPlaying == false)
		{
			StartCoroutine(MakeInactiveEventually());
		}
	}
	
	IEnumerator MakeInactiveEventually()
	{
		char[] ca = GameDataHolder.instance.ElementsUnlocked.ToCharArray();
		ca[elementID] = '1';
		GameDataHolder.instance.ElementsUnlocked = new string(ca);
		
		ElementalCube.ResetMat = true;
		
		isPlaying = true;
		GetComponent<MeshRenderer>().enabled = false;
		emitOnUnlocked.Play();
		for(int i = 0; i < 10; i++)
		{
			AudioSource.PlayClipAtPoint(playOnUnlock, transform.position);
			yield return new WaitForSeconds(0.5f);
		}
		yield return new WaitForSeconds(3f);
		DialogueHandler.RunEvent(dialogueOnFinished);
		gameObject.SetActive(false);
	}
}
