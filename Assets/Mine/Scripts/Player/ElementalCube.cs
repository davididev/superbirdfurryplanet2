﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementalCube : MonoBehaviour
{
	public int ElementID = 0;
	public MeshRenderer rend;
	
	public AudioClip shatterFX;
	
	public Material lockedMaterial, unlockedMaterial;
	
	public static bool ResetMat = false;
	
	public ParticleSystem[] hitExplosion;
	
	private float cubeTimer = 0f, resetMatTimer = 0f;
	private const float RESET_TIMER = 10f;
	
	private BoxCollider col;
	
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartupRoutine());
    }
	
	IEnumerator StartupRoutine()
	{
		yield return new WaitForSecondsRealtime(0.05f);
		while(UIPointer.IsActive == false)
		{
			yield return new WaitForSecondsRealtime(0.1f);
		}
		
		SetMat();
	}
	
	private void SetMat()
	{
		if(col == null)
			col = GetComponent<BoxCollider>();
		
		resetMatTimer = 0.1f;
		if(ElementID > 0)
		{
			char c = GameDataHolder.instance.ElementsUnlocked[ElementID-1];
			if(c == '1')
			{
				col.enabled = true;
				rend.material = unlockedMaterial;
			}
			else
			{
				rend.material = lockedMaterial;
				col.enabled = false;
			}
		}
	}

    // Update is called once per frame
    void Update()
    {
		if(col == null)
			col = GetComponent<BoxCollider>();
        if(ResetMat == true)
			SetMat();
		
		if(cubeTimer > 0f)
		{
			cubeTimer -= Time.deltaTime;
			if(cubeTimer <= 0f)
			{
				
				rend.enabled = true;
				col.enabled = true;
			}
		}
		
		if(resetMatTimer > 0f)
		{
			resetMatTimer -= Time.deltaTime;
			if(resetMatTimer <= 0f)
				ResetMat = false;	
		}
    }
	
	void OnDamage(DmgMessage msg)
	{
		if(col == null)
			col = GetComponent<BoxCollider>();
		cubeTimer = RESET_TIMER;
		rend.enabled = false;
		col.enabled = false;
		for(int i = 0; i < hitExplosion.Length; i++)
		{
			hitExplosion[i].Play();
		}
		FindObjectOfType<SwordController>().SetElement(ElementID);
		
		GameDataHolder.instance.MinMP = GameDataHolder.instance.MaxMP;
		AudioSource.PlayClipAtPoint(shatterFX, transform.position);
		MainUI.UpdateUI = true;
	}
}
