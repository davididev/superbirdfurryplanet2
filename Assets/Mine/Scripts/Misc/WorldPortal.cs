﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldPortal : MonoBehaviour
{
	public GameObject needPrefab;
    public int worldID = 0;
	public string sceneToMoveTo = "TheHavenVillage";
	public Vector3 positionToMoveTo = Vector3.zero;
	private bool hover = false;
	
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartMe());
    }
	
	IEnumerator StartMe()
	{
		yield return new WaitForSecondsRealtime(0.05f);
		while(UIPointer.IsActive == false)
		{
			yield return new WaitForSecondsRealtime(0.1f);
		}
		
		int likes = MissionsDB.Instance.worlds[worldID].likesNeeded;
		if(GameDataHolder.instance.Likes < likes)
		{
			GameObject inst = GameObject.Instantiate(needPrefab, transform.position, Quaternion.identity);
			inst.SendMessage("SetTxt", ("Need <color=yellow>" + likes + "</color>"));
			gameObject.SetActive(false);
		}
	}
	
	void OnDestroy()
	{
		if(hover)
		{
			MainUI.WorldName = "";
			hover = false;
			MainUI.UpdateUI = true;
		}
	}

    // Update is called once per frame
    void Update()
    {
		if(DialogueHandler.IS_RUNNING)
			return;

	   if(hover && Time.timeScale > 0.05f)
	   {
		   if(Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.VolumeUpButton))
		   {
			   MainUI.LoadScene(sceneToMoveTo, 2, positionToMoveTo);
		   }
	   }
	   
	   
    }
	
	void OnTriggerEnter(Collider c)
	{
		if(c.tag == "Player")
		{
			MainUI.WorldName = MissionsDB.Instance.worlds[worldID].worldName;
			hover = true;
			MainUI.UpdateUI = true;
		}
	}
	
	void OnTriggerExit(Collider c)
	{
		if(c.tag == "Player")
		{
			MainUI.WorldName = "";
			hover = false;
			MainUI.UpdateUI = true;
		}
	}
}
