﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FinchSlider : MonoBehaviour
{
	public float sliderValue = 1f;
	public float scaleSize = 20f;
	private BoxCollider col;
	
	private float hoverTimer = 0f;
	private Vector3 localHighlightPosition;
	
	public Transform sliderPosition;
	public UnityEvent OnPress;
	
    // Start is called before the first frame update
    void Start()
    {
        
    }
	
	public void OnHighlight(Vector3 pos)
	{
		hoverTimer = 0.1f;  //Stay hovered for three framesteps
		localHighlightPosition = transform.InverseTransformPoint(pos);
	}

    // Update is called once per frame
    void Update()
    {
		if(col == null)
			col = GetComponent<BoxCollider>();
		
        if(hoverTimer > 0f)
		{
			hoverTimer -= Time.unscaledDeltaTime;
			if(Finch.FinchController.Right.GetPress(Finch.FinchControllerElement.Trigger))
			{
				
				float x = localHighlightPosition.x / col.size.x;
				Debug.Log(localHighlightPosition.x + " size: " + col.size.x);
				float prevSliderValue = sliderValue;
				sliderValue = x + 0.5f;
				sliderValue = Mathf.Round(sliderValue * scaleSize) / scaleSize;
				
				
				if(!Mathf.Approximately(prevSliderValue, sliderValue))
					OnPress.Invoke();
				
			}
		}
		
		Vector3 ps = sliderPosition.localPosition;
		ps.x = col.size.x * (sliderValue - 0.5f);
		sliderPosition.localPosition = ps;
    }
}
