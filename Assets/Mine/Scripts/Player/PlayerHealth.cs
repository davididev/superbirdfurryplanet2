﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
	public AudioClip painSound, dieSound;
	private AudioSource painSrc;
	public static bool IsDamaged = false;
	private bool runningRoutine = false;
	private float blinkTimer = 0f;
	
	private int blinks = 0;
	public const int BLINKS_DMG = 20;
	public const float BLINK_TIMER = 0.5f;
	public SkinnedMeshRenderer rend;
	private CharacterControllerMovement ccm;
    // Start is called before the first frame update
    void Start()
    {
		painSrc = gameObject.AddComponent<AudioSource>();
        painSrc.clip = painSound;
		painSrc.minDistance = 10f;
		painSrc.playOnAwake = false;
    }
	
	public void OnDamage(DmgMessage m)
	{
		if(IsDamaged)
			return;
		
		if(ccm == null)
			ccm = GetComponent<CharacterControllerMovement>();
		GameDataHolder.instance.MinHP -= m.amount;
		
		IsDamaged = true;
		if(GameDataHolder.instance.MinHP <= 0)
		{
			painSrc.clip = dieSound;
			painSrc.Play();
			MainUI.LoadScene("GoldenRuler_Inside", 3, Vector3.zero);
		}
		else
		{
			painSrc.clip = painSound;
			painSrc.Play();
			if(m.normal != Vector3.zero)
			{
				ccm.AddForce(m.normal * 4f, 2f);
			}
		}
	}
	
	IEnumerator Blinka()
	{
		runningRoutine = true;
		int x = BLINKS_DMG;
		
		Color c1 = new Color(0f, 0f, 0f, 1f);
		Color c2 = new Color(0.5f, 0f, 0f, 1f);
		Color c3 = new Color(1f, 1f, 1f, 1f);
		Color c4 = new Color(0.5f, 0.5f, 0.5f, 1f);
		
		float t = BLINK_TIMER / 2f;
		
		
		iTween.ValueTo(gameObject, iTween.Hash("from", c1, "to", c2, "onupdate", "SetEmissive", "time", t));
		iTween.ValueTo(gameObject, iTween.Hash("from", c2, "to", c1, "onupdate", "SetEmissive", "time", t, "delay", t));
		yield return new WaitForSeconds(t * 2f);
		for(int i = 0; i < x - 3; i++)
		{
			iTween.ValueTo(gameObject, iTween.Hash("from", c1, "to", c3, "onupdate", "SetEmissive", "time", t * 2f / 3f));
			iTween.ValueTo(gameObject, iTween.Hash("from", c3, "to", c1, "onupdate", "SetEmissive", "time", t * 2f / 3f, "delay", t * 2f / 3f));
			yield return new WaitForSeconds(t * 2f);
		}
		for(int i = 0; i < 3; i++)
		{
			iTween.ValueTo(gameObject, iTween.Hash("from", c1, "to", c4, "onupdate", "SetEmissive", "time", t));
			iTween.ValueTo(gameObject, iTween.Hash("from", c4, "to", c1, "onupdate", "SetEmissive", "time", t, "delay", t));
			yield return new WaitForSeconds(t * 2f);
		}
		IsDamaged = false;
		runningRoutine = false;
	}
	
	private void SetEmissive(Color c)
	{
		
		if(rend != null)
            rend.material.SetColor("_EmissionColor", c);
	}

    // Update is called once per frame
    void Update()
    {
        if(IsDamaged && !runningRoutine)
			StartCoroutine(Blinka());
    }
}
