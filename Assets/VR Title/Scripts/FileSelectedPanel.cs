﻿using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FileSelectedPanel : MonoBehaviour
{
    public GameObject mainPanel, confirmDelete;
    public TMPro.TextMeshProUGUI fileStatus;
    int missionID = 0;
    // Start is called before the first frame update
    void OnEnable()
    {
        GameDataHolder.LoadFile(GameDataHolder.fileID);
		string s = "Likes: " + GameDataHolder.instance.Likes + "\n";
		s = s + "HP: " + GameDataHolder.instance.MinHP + "/" + GameDataHolder.instance.MaxHP + "\n";
		s = s + "MP: " + GameDataHolder.instance.MinMP + "/" + GameDataHolder.instance.MaxMP + "\n";
		fileStatus.text = s;
    }

    public void Continue()
    {
        //LoadingUI.sceneID = GameDataHolder.instance.sceneName;
        //UnityEngine.SceneManagement.SceneManager.LoadScene("Loading");
		SceneManager.LoadScene(GameDataHolder.instance.SceneName);
		FindObjectOfType<VRTitle>().gameObject.SetActive(false);
    }


    public void SetPanelID(int id)
    {
        mainPanel.SetActive(id == 0);
        confirmDelete.SetActive(id == 1);
        //confirmMission.SetActive(id >= 2 && id <= 3);
    }

    public void GoBack()
    {
        FindObjectOfType<VRTitle>().SetPanelID(0);
    }
    
    public void DeleteFile()
    {
        int id = GameDataHolder.fileID;
        string path1 = Application.persistentDataPath + "/file" + id + ".png";
        string path2 = Application.persistentDataPath + "/file" + id + ".dat";
        File.Delete(path1);
        if (File.Exists(path2))
            File.Delete(path2);
		mainPanel.SetActive(true);
		confirmDelete.SetActive(false);
        FindObjectOfType<VRTitle>().SetPanelID(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
