﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialHandler : MonoBehaviour
{
	static int Checkpoint = 0;
	public GameObject[] checkpoints;
	public TextAsset[] dialogueOnCheckpoint;
	private float restoreTimer = 0f;
    // Start is called before the first frame update
    void Start()
    {
		for(int i = 0; i < checkpoints.Length; i++)
		{
			checkpoints[i].SetActive(false);
		}
		
		StartCoroutine(StartFirstCheckpoint());
    }
	
	IEnumerator StartFirstCheckpoint()
	{
		yield return new WaitForSecondsRealtime(0.05f);
		while(UIPointer.IsActive == false)
		{
			yield return new WaitForSecondsRealtime(0.1f);
		}
		while(DialogueHandler.IS_RUNNING == true)
		{
			yield return new WaitForSecondsRealtime(0.1f);
		}
		
		SetCheckpoint(0);
	}
	
	public void SetCheckpoint(int check)
	{
		DialogueHandler.RunEvent(dialogueOnCheckpoint[check]);
		for(int i = 0; i < checkpoints.Length; i++)
		{
			checkpoints[i].SetActive(i == check);
		}
	}

    // Update is called once per frame
    void Update()
    {
        Restoration();
    }
	
	void Restoration()
	{
		restoreTimer += Time.deltaTime;
		if(restoreTimer > 0.1f)
		{
			GameDataHolder.instance.MinHP += 1;
			if(GameDataHolder.instance.MinHP > GameDataHolder.instance.MaxHP)
				GameDataHolder.instance.MinHP = GameDataHolder.instance.MaxHP;
			
			
			
			restoreTimer = 0f;
		}
	}
}
