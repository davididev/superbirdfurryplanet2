﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FinchButton : MonoBehaviour
{
	public UnityEvent OnPress;
	public Animator anim;
	
	public float repeatTime = 90f;
	private float pressTimer = 0f, hoverTimer = 0f;
	
	
    // Start is called before the first frame update
    void Start()
    {
        
    }
	
	//Message sent from UIPointer.cs
	public void OnHighlight(Vector3 pos)
	{
		hoverTimer = 0.1f;  //Stay hovered for three framesteps
		anim.SetBool("Hover", true);
	}

    // Update is called once per frame
    void Update()
    {
		if(pressTimer > 0f)
			pressTimer -= Time.unscaledDeltaTime;
		
        if(hoverTimer > 0f)
		{
			hoverTimer -= Time.unscaledDeltaTime;
			if(hoverTimer <= 0f)
			{
				pressTimer = 0f;
				anim.SetBool("Hover", false);
			}
			else
			{
				if(pressTimer <= 0f)
				{
					if(Finch.FinchController.Right.GetPress(Finch.FinchControllerElement.Trigger))
					{
						pressTimer = repeatTime;
						anim.SetTrigger("Click");
						OnPress.Invoke();
					}
				}
			}
		}
		
		
    }
}
