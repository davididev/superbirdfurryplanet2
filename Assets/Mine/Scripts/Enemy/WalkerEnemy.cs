﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkerEnemy : Enemy
{
    public override IEnumerator MainRoutine()
	{
		yield return new WaitForSeconds(0.5f);
		SetBrain(BRAIN.Wander);
		
		float playerDistance = 50f;
		while(playerDistance > 10f)
		{
			playerDistance = Vector3.Distance(transform.position, PlayerMovement.Position);
			yield return new WaitForSeconds(0.1f);
		}
		//Debug.Log("HELLO HELLO FGART");
		SetBrain(BRAIN.Follow);
	}
}
