﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DmgMessage 
{
    public int amount = 0;
	public int element = ELEMENT_NONE;
	public Vector3 hitPoint, normal;
	
	public DmgMessage(int a, int e, Vector3 pt, Vector3 n)
	{
		amount = a;
		element = e;
		hitPoint = pt;
		normal = n;
	}
	
	public const int ELEMENT_NONE = 0, ELEMENT_FIRE = 1, ELEMENT_ICE = 2, ELEMENT_SHADOW = 3, ELEMENT_LIGHTNING = 4, ELEMENT_COSMIC = 5;
}
