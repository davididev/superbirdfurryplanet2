﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

public class DialogueHandler : MonoBehaviour {

    public static Transform dialogueTarget;

    public GameObject messageBoxContainer, likeImage;
    private Image messageImage;
	public Image diagramImage;
	public Sprite[] diagrams;
    public Image characterImage, flashImage;
    public TMPro.TextMeshProUGUI message;
    public GameObject[] choiceOverlays;
    public int RUNNING_STACKS {
        get { return currentRunning.Count; } }


    public static AudioSource dialogueStringSrc;  //The source of regular dialogue

    public List<DialogueItem> currentRunning = new List<DialogueItem>();
    public DialogueHolder currentDialogue = null;
    private int currentID = -1;
    public static bool IS_RUNNING = false;

    // Use this for initialization
    void Start () {
        messageImage = messageBoxContainer.GetComponent<Image>();
        dialogueStringSrc = gameObject.AddComponent<AudioSource>();
        dialogueStringSrc.minDistance = 25f; 
        dialogueStringSrc.loop = false;
    }
	
	public void SetDiagram(int id)
	{
		if(id < 0)
			diagramImage.color = Color.clear;
		else
		{
			diagramImage.color = Color.white;
			diagramImage.sprite = diagrams[id];
		}
	}

    public void SetFlashColor(Color c)
    {
        flashImage.color = c;
    }

    /// <summary>
    /// Change the message Y.
    /// </summary>
    /// <param name="y">0 is showing, -192 is hidden.</param>
    public void SetMessageY(float y)
    {
        iTween.ValueTo(gameObject, iTween.Hash("from", messageImage.rectTransform.localScale, "to", new Vector3(y, y, y), "time", 0.25f, "onupdate", "SY", "ignoretimescale", true));
    }

    private void SY(Vector3 scale)
    {
        Vector3 v = messageImage.rectTransform.localScale;
        v = scale;
        messageImage.rectTransform.localScale = v;
    }

    public static Vector2 joystick1;

    public static Dictionary<string, float> variables = new Dictionary<string, float>();

    void OnDestroy()
    {
        dialogueTarget = null;
        variables.Clear();
        IS_RUNNING = false;
        
    }
	
	/// <summary>
    /// Get a temporary variable for the dialogue handler.
    /// </summary>
    /// <param name="varName">NAme of temporary variable</param>
    /// <returns></returns>
    public static float GetVar(string varName)
    {
        float f;
        if (variables.TryGetValue(varName, out f))
        {
            return f;
        }
        else
            return 0f;
    }

    /// <summary>
    /// Set a temporary variable for the dialogue handler.  Can only be read in if statements and dialogue.
    /// </summary>
    /// <param name="varName">Variable name to set</param>
    /// <param name="varValue">Float value to set it to.</param>
    public static void SetVar(string varName, float varValue)
    {
        if (variables.ContainsKey(varName))
            variables.Remove(varName);

        variables.Add(varName, varValue);
    }

    /// <summary>
    /// Pass in a string and get out the values of any variables that have been added.  Additionally checks for colors and new lines.
    /// </summary>
    /// <param name="str">String to check for variable names.</param>
    /// <returns></returns>
    public static string VariablesToString(string str)
    {
        str = str.Replace("\\c[0]", "</color>");
        str = str.Replace("\\c[1]", "<color=#ffff00>");
        str = str.Replace("\\c[2]", "<color=#ff0000>");
        str = str.Replace("\\n", "\n");
		str = str.Replace("#SO", "<sprite=\"Emoticans\" index=0>");
		str = str.Replace("#SN", "<sprite=\"Emoticans\" index=1>");
        Dictionary<string, float>.Enumerator e1 = DialogueHandler.variables.GetEnumerator();
        while(e1.MoveNext())
        {
            str = str.Replace(e1.Current.Key, e1.Current.Value.ToString());
        }
        return str;
    }

    private bool waitingForBreakpoint = false;
	// Update is called once per frame
	void Update () {
	    if(IS_RUNNING)
        {
			if(Finch.FinchController.Right.GetPress(Finch.FinchControllerElement.ThumbButton))
				joystick1 = Finch.FinchController.Right.TouchAxes.normalized;
			else
			{
				float ty = 0f;
				if(Finch.FinchController.Right.GetPress(Finch.FinchControllerElement.VolumeUpButton))
					ty++;
				if(Finch.FinchController.Right.GetPress(Finch.FinchControllerElement.VolumeDownButton))
					ty--;
				joystick1 = new Vector2(0f, ty);
			}
            
            if (waitingForBreakpoint == false)
            {
                
                //Keep going until you reach a breakpoint or the end
                currentID++;
                if(currentID >= currentDialogue.items.Length)  //At the end
                {
                    EndEvent();
                    return;
                }
                
                
                Debug.Log("<B>ADDING EVENT" + currentDialogue.items[currentID].ToString() + "</b>");
                StartCoroutine(currentDialogue.items[currentID].Run());
                currentRunning.Add(currentDialogue.items[currentID]);
                if (currentDialogue.items[currentID].breakpoint == true)
                {
                    Debug.Log("<i>Halt, breakpoint at line " + currentID + ": " + currentDialogue.items[currentID].ToString() + " </i>");
                    waitingForBreakpoint = true;
                }
            }
            else
            {
                //One of the items had a breakpoint.  Wait until all of them are completed before continuing.
                /*
                List<DialogueItem>.Enumerator e1 = currentRunning.GetEnumerator();
                while (e1.MoveNext())
                {
                    if (e1.Current.completed == true)
                    {
                        currentRunning.Remove(e1.Current);
                        break;
                    }
                }
                */
                bool b = true;
                while(b == true) { b = RemoveCurrentRunning(); }

                if (currentRunning.Count == 0) //All items removed
                {
                    waitingForBreakpoint = false;
                    if(gotoL != -1)  //The breakpoint called for a goto line
                    {
                        currentID = gotoL - 1;
                        gotoL = -1;
                        waitingForBreakpoint = false;
                    }
                }
            }
        }
	}
    
    /// <summary>
    /// Remove any currentRunning scripts that are finished (should be called multiple times)
    /// </summary>
    /// <returns>"true" if there  are still some that need to be removed, else returns "false"</returns>
    public bool RemoveCurrentRunning()
    {
        List<DialogueItem>.Enumerator e1 = currentRunning.GetEnumerator();
        while (e1.MoveNext())
        {
            if (e1.Current.completed == true)
            {
                currentRunning.Remove(e1.Current);
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Write the global data variables to variable system (should be called before Dialogue Handler starts)
    /// </summary>
    void SetGlobalVars()
    {
		if(MapVars.WorldID > -1)
		{
			GameData.WorldInfo info = GameDataHolder.instance.Worlds[MapVars.WorldID];
			for (int i = 0; i < info.MissionsProgress.Length; i++)
			{
				variables.Remove("%mis" + i);
				float f = info.MissionsProgress[i];
				variables.Add("%mis" + i, f);
			}
		}
		
		
		int x = 0;
		for(int i = 0; i < GameDataHolder.instance.items.Length; i++)
		{
			if(GameDataHolder.instance.items[i] == "0")
				x++;
		}
		variables.Remove("%emptyItems");
		variables.Add("%emptyItems", (float) x);
		variables.Remove("%likes");
		variables.Add("%likes", (float) GameDataHolder.instance.Likes);
		variables.Remove("%niara");
		variables.Add("%niara", (float) GameDataHolder.instance.Niara);
		variables.Remove("%shopPoints");
		variables.Add("%shopPoints", (float) GameDataHolder.instance.ShopPoints);

		float ele = (float) FindObjectOfType<SwordController>().elementID;
		variables.Remove("%element");
		variables.Add("%element", ele);
    }


    /// <summary>
    /// Get the global data variables from variable system (should be called at the end of Dialogue Hansdler)
    /// </summary>
    void GetGlobalVars()
    {
        
		if(MapVars.WorldID > -1)
		{
            float f2 = 0f;
            GameData.WorldInfo info = GameDataHolder.instance.Worlds[MapVars.WorldID];
			for (int i = 0; i < info.MissionsProgress.Length; i++)
            {
                if(variables.TryGetValue("%mis" + i, out f2))
				{
					info.MissionsProgress[i] = f2;
				}
            }
			
			GameDataHolder.instance.Worlds[MapVars.WorldID] = info;
			float n = 0f;
			if(variables.TryGetValue("%niara", out n))
				GameDataHolder.instance.Niara = (int) n;
			
			n = 0f;
			if(variables.TryGetValue("%shopPoints", out n))
				GameDataHolder.instance.ShopPoints = (int) n;
        }

        

    }
	
	/// <summary>
    /// Obtain gameobject with tag and Start event based on text asset.
    /// </summary>
    /// <param name="file"></param>
	public static void RunEvent(TextAsset file)
	{
		DialogueHandler dm = GameObject.FindWithTag("Dialogue Handler").GetComponent<DialogueHandler>();
		dm.StartEvent(file);
	}
	
	
	/// <summary>
    /// Obtain gameobject with tag and Start event based on classes
    /// </summary>
    /// <param name="items"></param>
	public static void RunEvent(DialogueItem[] items)
	{
		DialogueHandler dm = GameObject.FindWithTag("Dialogue Handler").GetComponent<DialogueHandler>();
		dm.StartEvent(items);
	}
	
    /// <summary>
    /// Start event based on text asset.
    /// </summary>
    /// <param name="file"></param>
    public void StartEvent(TextAsset file)
    {
        SetGlobalVars();
        var serializer = new XmlSerializer(typeof(DialogueHolder)); 
        currentDialogue = serializer.Deserialize(new StringReader(file.text)) as DialogueHolder;
        LoadedDialogueHolder();
    }

    /// <summary>
    /// Start event based on classes
    /// </summary>
    /// <param name="items"></param>
    public void StartEvent(DialogueItem[] items)
    {
        SetGlobalVars();
        DialogueHolder dh = new DialogueHolder();
        dh.items = items;
        currentDialogue = dh;
        LoadedDialogueHolder();
    }

    /// <summary>
    /// internal function.  Use after DialogueHolder is loaded.
    /// </summary>
    void LoadedDialogueHolder()
    {
        waitingForBreakpoint = false;
        currentID = -1;
        IS_RUNNING = true;

    }

    //When all the breakpoints are over, go to this line
    private int gotoL = -1;
    public void GotoLine(int i)
    {
        gotoL = i;
    }
    
    public void EndEvent()
    {
        Debug.Log("<B>END EVENT</b>");
        GetGlobalVars();
		
		
        currentRunning.Clear();
        currentID = -1;
        IS_RUNNING = false;
		
		if(GameDataHolder.instance.ShopPoints == 10)
		{
			GameDataHolder.instance.ShopPoints++;
			GameDataHolder.instance.NiaraUpgrades++;
			StartEvent(Resources.Load<TextAsset>("wallet"));
		}
		if(GameDataHolder.instance.ShopPoints == 100)
		{
			GameDataHolder.instance.ShopPoints++;
			GameDataHolder.instance.NiaraUpgrades++;
			StartEvent(Resources.Load<TextAsset>("wallet"));
		}
		if(GameDataHolder.instance.ShopPoints == 800)
		{
			GameDataHolder.instance.ShopPoints++;
			GameDataHolder.instance.NiaraUpgrades++;
			StartEvent(Resources.Load<TextAsset>("wallet"));
		}
    }
}
