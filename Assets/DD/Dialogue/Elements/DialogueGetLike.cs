﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueGetLike : DialogueItem
{
    public DialogueGetLike()
    {
        breakpoint = true;
    }

    public override IEnumerator Run()
    {
        yield return base.Run();
        handle.likeImage.SetActive(true);
		handle.likeImage.transform.localScale = Vector3.zero;
		AudioClip clip = Resources.Load<AudioClip>("Like");
		AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
		
		iTween.ScaleTo(handle.likeImage, Vector3.one, 0.2f);
		GameDataHolder.instance.Likes++;
		yield return new WaitForSeconds(4f);
		iTween.ScaleTo(handle.likeImage, Vector3.zero, 0.2f);
		yield return new WaitForSeconds(0.2f);
		handle.likeImage.SetActive(false);
        completed = true;
        yield return null;
    }

    public override void DrawEditor(Rect verticalGroup)
    {

        GUILayout.Label("This element is not editable.");
    }

    //Needed for the editor.  Returns the object tpye
    public override string ToString()
    {
        return "Get Like!";
    }
}
