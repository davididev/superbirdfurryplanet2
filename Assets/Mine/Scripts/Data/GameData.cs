﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
[System.Serializable]
public class GameData 
{
    [SerializeField] public string SceneName;
	
	[SerializeField] public int MinHP = 9, MinMP = 5, MinFeathers = 5, AttackUpgrades = 1, HPUpgrades = 0, MPUpgrades = 0, FeatherUpgrades = 0, Likes = 0;
	[SerializeField] public int Niara = 0, NiaraUpgrades = 0, BeserkCount = 0, SelectedRelic = -1;
	[SerializeField] public int ShopPoints = 0;
	[SerializeField] public float VolumeSound = 1f, VolumeMusic = 1f;
	[SerializeField] public string walletUpgrades = "000", unlockedRelics = "0000";
	[SerializeField] public string[] items = {"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"};
	//The relics: 
		//0: Double MP recovery
		//1: Fast feather recovery
		//2: Half Damage
		//3: Quicker Beserk Mode
	
	
	public int MaxNiara
	{
		get 
		{
			if(NiaraUpgrades == 0)
				return 99;
			if(NiaraUpgrades == 1)
				return 200;
			if(NiaraUpgrades == 2)
				return 500;
			
			return 999;
		}
	}

	public int MaxHP
	{
		get { int h = 9;  h += HPUpgrades * 3; return h; }
	}
	public int MaxMP
	{
		get { int m = 5;  m += MPUpgrades * 5; return m; }
	}
	
	public int MaxFeathers
	{
		get { int f = 5; f += FeatherUpgrades * 5;  return f; }
	}
	
	[SerializeField] public string ElementsUnlocked = "00000";
	
	[System.Serializable]
	public class WorldInfo
	{
		[SerializeField] public string Powerups = "0000";  //HP Upgrade, MP Upgrade, Feather Upgrade, Attack upgrade
		[SerializeField] public float[] MissionsProgress = {0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f};
		[SerializeField] public bool GotTreasure = false;
		[SerializeField] public string Warps = "00000000";
		
	}
	
	[SerializeField] public WorldInfo[] Worlds;
	
	public GameData()
	{
		SceneName = "Intro";
		Worlds = new WorldInfo[8];
		for(int i = 0; i < Worlds.Length; i++)
		{
			Worlds[i] = new WorldInfo();
		}
	}
}
