﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueChangeScene : DialogueItem
{
	public string sceneToMoveTo = "SampleScene";
	public int transitionType = 1;
	public Vector3 positionToMoveTo = Vector3.zero;
	
    public DialogueChangeScene()
    {
        breakpoint = true;
    }
	
	public override IEnumerator Run()
    {
        yield return base.Run();
		MainUI.LoadScene(sceneToMoveTo, transitionType, positionToMoveTo);
		yield return new WaitForSecondsRealtime(0.1f);
		completed = true;
	}
	
	public override void DrawEditor(Rect verticalGroup)
    {
		GUILayout.Label("Scene to move to");
		sceneToMoveTo = GUILayout.TextField(sceneToMoveTo);
		
		transitionType = this.IntField("Transition type", transitionType);
		
		GUILayout.BeginHorizontal();
		positionToMoveTo.x = this.FloatField("x", positionToMoveTo.x);
		positionToMoveTo.y = this.FloatField("y", positionToMoveTo.y);
		positionToMoveTo.z = this.FloatField("z", positionToMoveTo.z);
		GUILayout.EndHorizontal();
		
	}
}
