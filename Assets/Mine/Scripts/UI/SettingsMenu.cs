﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SettingsMenu : MonoBehaviour
{
	public FinchSlider musicVolume, soundVolume;
	public TMPro.TextMeshProUGUI saveText, quitText, portalRoomText;
	
	private int quitCount = 4, portalRoomCount = 4;
	private float quitSaveTimer = 0f;
	public GameObject portalRoomButton;
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

	public void Refresh()
	{
		musicVolume.sliderValue = GameDataHolder.instance.VolumeMusic;
		soundVolume.sliderValue = GameDataHolder.instance.VolumeSound;
		
		Debug.Log("Edit portal room button later.");
		//portalRoomButton.SetActive(SceneManager.GetActiveScene().buildIndex > 3);
		
	}
	

    // Update is called once per frame
    void Update()
    {
        if(quitSaveTimer > 0f)
		{
			quitSaveTimer -= Time.unscaledDeltaTime;
			if(quitSaveTimer <= 0f)
			{
				quitCount = 4;
				portalRoomCount = 4;
				quitText.text = "Quit";
				saveText.text = "";
				portalRoomText.text = "Back to\nPortal Room";
			}
		}
    }
	
	public void SaveGame()
	{
		try
		{
			GameDataHolder.instance.SceneName = "GoldenRuler_Outside";
		GameDataHolder.SaveFile(GameDataHolder.fileID);
		saveText.text = "Game saved!";
		}
		catch(System.Exception e)
		{
			saveText.text = "Error saving.\nTry again.";
		}
		quitSaveTimer = 2f;
	}
	
	public void PressQuitButton()
	{
		quitSaveTimer = 1.25f;
		quitCount -= 1;
		if(quitCount == 0)
		{
			SceneManager.LoadScene("Title");
		}
		else
			quitText.text = "In " + quitCount + "...";
		
	}
	
	public void PressPortalRoom()
	{
		Scene s = SceneManager.GetActiveScene();
		if(s.name == "Tutorial")
		{
			portalRoomText.text = "Not yet";
			return;
		}
		quitSaveTimer = 1.25f;
		portalRoomCount -= 1;
		if(portalRoomCount == 0)
		{
			MainUI.LoadScene("GoldenRuler_Inside", 1, Vector3.zero);
			transform.parent.gameObject.SendMessage("Close");
		}
		else
			portalRoomText.text = "Going back\nIn " + portalRoomCount + "...";
		
	}
	
	public void UpdateVolume()
	{
		GameDataHolder.instance.VolumeMusic = musicVolume.sliderValue;
		GameDataHolder.instance.VolumeSound = soundVolume.sliderValue;
		
		PlayMusic.UpdateVolume = true;
		
	}
}
