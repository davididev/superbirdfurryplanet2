﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapVars : MonoBehaviour
{
	public AudioClip introMusic, loopMusic;
	public GameObject boilerplatePrefab;
	public int worldID = -1;
	public TextAsset dialogueOnStart;
	
	public static int WorldID;
	
	public float _deathY = -5000f;
	public static float DeathY = -5000f;
    // Start is called before the first frame update
    void Start()
    {
		DeathY = _deathY;
		WorldID = worldID;
		if(FindObjectOfType<BoilerRoot>() == null)
		{
			GameObject g = Instantiate(boilerplatePrefab, Vector3.zero, Quaternion.identity) as GameObject;
		}
        
		StartCoroutine(StartMusic());
		if(dialogueOnStart != null)
			StartCoroutine(StartDialogue());
    }
	
	void OnDestroy()
	{
		DeathY = -5000f;
	}
	
	IEnumerator StartMusic()
	{
		yield return new WaitForSecondsRealtime(1f);
		FindObjectOfType<PlayMusic>().PlaySong(loopMusic, introMusic);
	}
	
	IEnumerator StartDialogue()
	{
		yield return new WaitForSecondsRealtime(0.05f);
		while(UIPointer.IsActive == false)
		{
			yield return new WaitForSecondsRealtime(0.1f);
		}
		while(DialogueHandler.IS_RUNNING == true)
		{
			yield return new WaitForSecondsRealtime(0.1f);
		}
		CharacterControllerMovement ccm = GameObject.FindWithTag("Player").GetComponent<CharacterControllerMovement>();
		while(ccm.IsGrounded == false)
			{
			yield return new WaitForSecondsRealtime(0.1f);
		}
		DialogueHandler.RunEvent(dialogueOnStart);
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
