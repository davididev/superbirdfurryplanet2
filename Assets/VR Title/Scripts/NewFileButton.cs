﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewFileButton : MonoBehaviour
{
    private int fileID = -1;
    private bool isNew = false;
    public Texture defaultTexture;
    // Start is called before the first frame update
    void OnEnable()
    {
        if (fileID != -1)
            SetFileID(fileID);
    }

    public void SetFileID(int id)
    {
        fileID = id;
        string path = Application.persistentDataPath + "/file" + id + ".png";
        if (System.IO.File.Exists(path))
        {
            isNew = false;
            var bytes = System.IO.File.ReadAllBytes(path);
            var tex = new Texture2D(1, 1);
            tex.LoadImage(bytes);

            GetComponent<RawImage>().texture = tex;
        }
        else
        {
            isNew = true;
            GetComponent<RawImage>().texture = defaultTexture;
        }
    }

    public void Press()
    {
        GameDataHolder.fileID = fileID;
        if(isNew)
        {
            FindObjectOfType<VRTitle>().SetPanelID(1);
        }
        else
        {
            FindObjectOfType<VRTitle>().SetPanelID(2);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
