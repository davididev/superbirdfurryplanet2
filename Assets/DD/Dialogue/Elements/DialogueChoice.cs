﻿using System.Collections;
using System.Xml.Serialization;
using UnityEngine;

public class DialogueChoice : DialogueItem
{
    public string promptHeader = "";
    //public string[] choices = new string[0];
    [System.Serializable]
    public class Choice
    {
        public string choiceStr = "";
        public int gotoLine = 0;
    }
    public Choice[] choices = new Choice[0];

    [XmlIgnore] private int currentChoiceID = 0;

    public DialogueChoice()
    {
        breakpoint = true;
    }

    public override IEnumerator Run()
    {
        yield return base.Run();
        //Startup
		handle.SetMessageY(1f);
		handle.characterImage.sprite = Resources.Load<Sprite>("Face/Superbird");
        float lastTimeScale = Time.timeScale;
        Time.timeScale = 1f / 360f;


        
        string str = promptHeader;
        foreach(Choice c in choices)
        {
            str = str + "\n" + c.choiceStr;
        }
        str = DialogueHandler.VariablesToString(str);

        

        handle.message.text = str;
        int currentChoiceID = 0;
        bool selectionPressed = false;
        bool continueMe = true;

        for (int i = 0; i < handle.choiceOverlays.Length; i++)
        {
            handle.choiceOverlays[i].SetActive(currentChoiceID == i);
        }

        while (Finch.FinchController.Right.GetPress(Finch.FinchControllerElement.Trigger)== true)
        { yield return null; }//So any dialogue before doesn't make the choice selected instantly

        while (continueMe)
        {
            float selection = DialogueHandler.joystick1.y;
            Debug.Log("Selection: " + selection);
            if (selectionPressed == true && Mathf.Approximately(selection, 0f))
                selectionPressed = false;

            if (selection < 0 && selectionPressed == false)
            { currentChoiceID++; selectionPressed = true; }
            if (selection > 0 && selectionPressed == false)
            { currentChoiceID--; selectionPressed = true; }


            if (currentChoiceID < 0)
                currentChoiceID = choices.Length - 1;
            if(currentChoiceID >= choices.Length)
                currentChoiceID = 0;

            for(int i = 0; i < handle.choiceOverlays.Length; i++)
            {
                handle.choiceOverlays[i].SetActive(currentChoiceID == i);
            }

            if (Finch.FinchController.Right.GetPress(Finch.FinchControllerElement.Trigger)== true)
                continueMe = false;

            yield return null;
        }

        //Clear choices after the choice
        for (int i = 0; i < handle.choiceOverlays.Length; i++)
        {
            handle.choiceOverlays[i].SetActive(false);
        }

        Time.timeScale = lastTimeScale;

        handle.GotoLine(choices[currentChoiceID].gotoLine);
        
        completed = true;
    }

    private int lastChoiceCount = 0;
    private int t = 0;  //t is set per OnGUI
    public override void DrawEditor(Rect verticalGroup)
    {
        GUILayout.Label("Top prompt");
        promptHeader = GUILayout.TextField(promptHeader);
        t = this.IntField("choice count", t);
        if(t != lastChoiceCount)  //Change size
        {
            choices = null;
            choices = new Choice[t];
            for(int i = 0; i < choices.Length; i++)
            {
                choices[i] = new Choice();
            }
            lastChoiceCount = t;
        }

        for(int i = 0; i < choices.Length; i++)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Choice " + i);
            
            choices[i].choiceStr = GUILayout.TextField(choices[i].choiceStr);
            choices[i].gotoLine = this.IntField("Goto", choices[i].gotoLine);
            GUILayout.EndHorizontal();
        }
    }

    public override string ToString()
    {
        return "Set choice";
    }
}