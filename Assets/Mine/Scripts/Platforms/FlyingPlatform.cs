﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingPlatform : MonoBehaviour
{
	public ParticleSystem ps;
	private bool lastRunning = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(lastRunning)
		{
			if(!PlayerMovement.IsRunning)
			{
				lastRunning = false;
				ps.Stop();
			}
		}
		else
		{
			if(PlayerMovement.IsRunning)
			{
				lastRunning = true;
				ps.Play();
			}
		}
    }
	
	void OnTriggerEnter(Collider c)
	{
		if(c.tag == "Player")
		{
			if(lastRunning)
			{
				c.gameObject.SendMessage("StartFlying");
			}
		}
	}
}
