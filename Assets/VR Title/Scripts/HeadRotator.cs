﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadRotator : MonoBehaviour
{
    private const float ROTATE_PER_SECOND = 90f;
    [SerializeField] public Transform mainCamera, uiCamera, uiHead;
    public static Vector3 RealFacing;  //Accounting for VR rotation and head rotation
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

		Vector3 rotVec = transform.eulerAngles;
		
		//Controller turning
		Vector2 moveVec = Finch.FinchController.Right.TouchAxes;
		if(!Finch.FinchController.Right.GetPress(Finch.FinchControllerElement.ThumbButton))
		{
			if(moveVec.x <= -0.33f)
				rotVec.y -= ROTATE_PER_SECOND * 2f / 3f * Time.unscaledDeltaTime;
			if(moveVec.x >= 0.33f)
				rotVec.y += ROTATE_PER_SECOND * 2f / 3f * Time.unscaledDeltaTime;
		}
#if UNITY_STANDALONE || UNITY_EDITOR
        //Only rotate by tilting phone in editor.
        float rot = Input.GetAxis("Horizontal");
        
        if (rot > 0.25f)
            rotVec.y += ROTATE_PER_SECOND * Time.unscaledDeltaTime;
        if (rot < -0.25f)
            rotVec.y -= ROTATE_PER_SECOND * Time.unscaledDeltaTime;

        float xVec = Input.GetAxis("Vertical");
        //rotVec.x = Mathf.MoveTowardsAngle(rotVec.x, xVec, ROTATE_PER_SECOND * Time.unscaledDeltaTime);
        if (xVec < 0f)
            rotVec.x = Mathf.MoveTowardsAngle(rotVec.x, 88f, ROTATE_PER_SECOND * Time.unscaledDeltaTime);
        if (xVec > 0f)
            rotVec.x = Mathf.MoveTowardsAngle(rotVec.x, -88f, ROTATE_PER_SECOND * Time.unscaledDeltaTime);
        
#endif
		transform.eulerAngles = rotVec;
        RealFacing = mainCamera.eulerAngles;
		
		
		
    }
}
