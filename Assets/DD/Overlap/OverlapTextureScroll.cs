﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverlapTextureScroll : MonoBehaviour
{
    private MeshRenderer rend;
	private SkinnedMeshRenderer rend2;

    public Vector2 scrollRateMain, scrollRateSecondary;

    private Vector2 currentScrollMain, currentScrollSecondary;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (rend == null)
            rend = GetComponent<MeshRenderer>();
		if(rend == null && rend2 == null)
			rend2 = GetComponent<SkinnedMeshRenderer>();
        currentScrollMain += (scrollRateMain * Time.deltaTime);
        currentScrollSecondary += (scrollRateSecondary * Time.deltaTime);
        if (currentScrollMain.x > 1f)
            currentScrollMain.x -= 1f;
        if (currentScrollMain.x < 0f)
            currentScrollMain.x += 1f;
        if (currentScrollMain.y > 1f)
            currentScrollMain.y -= 1f;
        if (currentScrollMain.y < 0f)
            currentScrollMain.y += 1f;
        if (currentScrollSecondary.x > 1f)
            currentScrollSecondary.x -= 1f;
        if (currentScrollSecondary.x < 0f)
            currentScrollSecondary.x += 1f;
        if (currentScrollSecondary.y > 1f)
            currentScrollSecondary.y -= 1f;
        if (currentScrollSecondary.y < 0f)
            currentScrollSecondary.y += 1f;
        //Debug.Log("Scroll: " + currentScrollMain + " " + currentScrollSecondary);
        
		if(rend != null)
		{
			rend.material.SetTextureOffset("_MainTex", currentScrollMain);
			rend.material.SetTextureOffset("_OverlapTex", currentScrollSecondary);
		}
		if(rend2 != null)
		{
			rend2.material.SetTextureOffset("_MainTex", currentScrollMain);
			rend2.material.SetTextureOffset("_OverlapTex", currentScrollSecondary);
		}
    }
}
