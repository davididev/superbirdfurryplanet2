﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
	private const float SCALE_TIME = 0.25f, ROTATE_PER_SECOND = 360f, REFRESH_TIMER = 0.2f;
	private float targetAngle = 0f;
	
	
	public GameObject[] panels;
	public GameObject leftButton, rightButton;
    // Start is called before the first frame update
    void OnEnable()
    {
        Time.timeScale = 1f / 360f;
		iTween.ScaleTo(gameObject, iTween.Hash("scale", new Vector3(1f, 1f, 1f), "time", SCALE_TIME, "ignoretimescale", true));
		leftButton.SetActive(true);
		rightButton.SetActive(true);
		RefreshCurrentPanel();
    }
	
	private float refreshedTimer = 0f;
	void RefreshCurrentPanel()
	{
		if(Mathf.Abs(Mathf.DeltaAngle(transform.localEulerAngles.y, 0f)) < 2f)
		{
			refreshedTimer = 0f;
			panels[0].SendMessage("Refresh", SendMessageOptions.DontRequireReceiver);
			for(int i = 0; i < panels[0].transform.childCount; i++)
			{
				panels[0].transform.GetChild(i).gameObject.SendMessage("Refresh", SendMessageOptions.DontRequireReceiver);
			}
		}
		if(Mathf.Abs(Mathf.DeltaAngle(transform.localEulerAngles.y, 90f)) < 2f)
		{
			refreshedTimer = 0f;
			panels[1].SendMessage("Refresh", SendMessageOptions.DontRequireReceiver);
		}
		if(Mathf.Abs(Mathf.DeltaAngle(transform.localEulerAngles.y, 180f)) < 2f)
		{
			refreshedTimer = 0f;
			panels[2].SendMessage("Refresh", SendMessageOptions.DontRequireReceiver);
		}
		if(Mathf.Abs(Mathf.DeltaAngle(transform.localEulerAngles.y, 270f)) < 2f)
		{
			refreshedTimer = 0f;
			panels[3].SendMessage("Refresh", SendMessageOptions.DontRequireReceiver);
		}
	}
	
	void OnDisable()
	{
		Time.timeScale = 1f;
	}
	
	public void Close()
	{
		StartCoroutine(CloseMenu());
	}
	
	public void TurnWindowLeft()
	{
		if(Mathf.Abs(Mathf.DeltaAngle(transform.localEulerAngles.y, targetAngle)) < 5f)  //Only do it if close to target angle.
		{
			refreshedTimer = REFRESH_TIMER;
			targetAngle -= 90f;
		}
		if(targetAngle < 0f)
			targetAngle += 360f;
	}
	
	public void TurnWindowRight()
	{
		if(Mathf.Abs(Mathf.DeltaAngle(transform.localEulerAngles.y, targetAngle)) < 5f)
		{
			refreshedTimer = REFRESH_TIMER;
			targetAngle += 90f;
		}
		
		
		if(targetAngle > 360)
			targetAngle -= 360f;
	}
	
	private IEnumerator CloseMenu()
	{
		leftButton.SetActive(false);
		rightButton.SetActive(false);
		iTween.ScaleTo(gameObject, iTween.Hash("scale", new Vector3(1f, 0.1f, 1f), "time", SCALE_TIME, "ignoretimescale", true));
		yield return new WaitForSecondsRealtime(SCALE_TIME);
		gameObject.SetActive(false);
	}

    // Update is called once per frame
    void Update()
    {
        if(UIPointer.IsActive == false)  //Lost connection.  Let's not crash the game.
			Close();
			
		if(Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.VolumeDownButton))
			TurnWindowLeft();
		if(Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.VolumeUpButton))
			TurnWindowRight();
		
		
		Vector3 rot = transform.localEulerAngles;
		rot.y = Mathf.MoveTowardsAngle(rot.y, targetAngle, ROTATE_PER_SECOND * Time.unscaledDeltaTime);
		transform.localEulerAngles = rot;
		
		if(refreshedTimer > 0f)
		{
			if(Mathf.Abs(Mathf.DeltaAngle(transform.localEulerAngles.y, targetAngle)) < 25f)  //Only do it if close to target angle.
			{
				refreshedTimer -= Time.unscaledDeltaTime;
				if(refreshedTimer <= 0f)
					RefreshCurrentPanel();
			}
		}
    }
}
