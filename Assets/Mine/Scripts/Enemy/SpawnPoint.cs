﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
	private float respawnTimer = 0f;
	//private const float RESPAWN_TIME = 5f;		//The test time
	private const float RESPAWN_TIME = 30f; 	//The real time
	private bool isInvisible = false;
	
    // Start is called before the first frame update
    void Start()
    {
        
    }
	
	void OnBecameInvisible()
	{
		isInvisible = true;
	}
	
	void OnBecameVisible()
	{
		isInvisible = false;
	}

    // Update is called once per frame
    void Update()
    {
        bool childActive = transform.GetChild(0).gameObject.activeSelf;
		
		if(!childActive && isInvisible)
		{
			respawnTimer += Time.deltaTime;
			if(respawnTimer >= RESPAWN_TIME)
			{
				respawnTimer = 0f;
				GameObject g = transform.GetChild(0).gameObject;
				g.SetActive(true);
				g.transform.localPosition = Vector3.zero;
			}
		}
    }
}
