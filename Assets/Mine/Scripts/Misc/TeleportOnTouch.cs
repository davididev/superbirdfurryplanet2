﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportOnTouch : MonoBehaviour
{
	public AudioClip transitionSound;
	public Animator anim;
	public string animTrigger;
	
	public Vector3 newPos;
	public string newScene;
	public int transitionID = 1;
	private bool isTeleported = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	void OnTriggerEnter(Collider col)
	{
		if(isTeleported)
			return;
		if(col.tag == "Player")
		{
			if(anim != null)
				anim.SetTrigger(animTrigger);
			isTeleported = true;
			MainUI.LoadScene(newScene,transitionID, newPos, transitionSound);
		}
	}
}
