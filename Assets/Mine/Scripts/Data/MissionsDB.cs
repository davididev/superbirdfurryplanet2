﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionsDB 
{
    public static MissionsDB Instance = new MissionsDB();
	
	public class World
	{
		public string[] missionTexts;
		public string worldName;
		public int likesNeeded = 0;
		
		public World()
		{
			missionTexts = new string[10];
		}
		
		public void SetWorldMission(int id, string s)
		{
			missionTexts[id] = s;
		}
	}
	
	public World[] worlds;
	
	
	public MissionsDB()
	{
		worlds = new World[8];
		for(int i = 0; i < worlds.Length; i++)
		{
			worlds[i] = new World();
		}
		
		//World 1: The Haven
		worlds[0].worldName = "The Haven";
		worlds[0].likesNeeded = 1;
		worlds[0].SetWorldMission(0, "Buy Moss Coffee.");
		worlds[0].SetWorldMission(1, "Rescue Jaiden from the forest.");
		worlds[0].SetWorldMission(2, "Beat Floof Cop's Shooting Game");
		worlds[0].SetWorldMission(3, "Defeat King ROFLCopter.");
		worlds[0].SetWorldMission(4, "Talk down Coolperez");
		worlds[0].SetWorldMission(5, "Fence with Yure");
		worlds[0].SetWorldMission(6, "Race with Nuez's rubber ball");
		worlds[0].SetWorldMission(7, "Cool down Anxel's hot head");
		worlds[0].SetWorldMission(8, "Get Blado chicken nuggets");
		worlds[0].SetWorldMission(9, "???");
		
		
		//World 2: The Desert
		worlds[1].worldName = "Thirstquencher Field";
		worlds[1].likesNeeded = 3;
		worlds[1].SetWorldMission(0, "???");
		worlds[1].SetWorldMission(1, "???");
		worlds[1].SetWorldMission(2, "???");
		worlds[1].SetWorldMission(3, "???");
		worlds[1].SetWorldMission(4, "???");
		worlds[1].SetWorldMission(5, "???");
		worlds[1].SetWorldMission(6, "???");
		worlds[1].SetWorldMission(7, "???");
		worlds[1].SetWorldMission(8, "???");
		worlds[1].SetWorldMission(9, "???");
		
		//World 3: Fire & Ice
		worlds[2].worldName = "Brr Erst Foot";
		worlds[2].likesNeeded = 8;
		worlds[2].SetWorldMission(0, "???");
		worlds[2].SetWorldMission(1, "???");
		worlds[2].SetWorldMission(2, "???");
		worlds[2].SetWorldMission(3, "???");
		worlds[2].SetWorldMission(4, "???");
		worlds[2].SetWorldMission(5, "???");
		worlds[2].SetWorldMission(6, "???");
		worlds[2].SetWorldMission(7, "???");
		worlds[2].SetWorldMission(8, "???");
		worlds[2].SetWorldMission(9, "???");
		
		//World 4: Dark Land
		worlds[3].worldName = "Twilight Field";
		worlds[3].likesNeeded = 12;
		worlds[3].SetWorldMission(0, "???");
		worlds[3].SetWorldMission(1, "???");
		worlds[3].SetWorldMission(2, "???");
		worlds[3].SetWorldMission(3, "???");
		worlds[3].SetWorldMission(4, "???");
		worlds[3].SetWorldMission(5, "???");
		worlds[3].SetWorldMission(6, "???");
		worlds[3].SetWorldMission(7, "???");
		worlds[3].SetWorldMission(8, "???");
		worlds[3].SetWorldMission(9, "???");
		
		//World 5: Water World
		worlds[4].worldName = "Lost Lagoon";
		worlds[4].likesNeeded = 20;
		worlds[4].SetWorldMission(0, "???");
		worlds[4].SetWorldMission(1, "???");
		worlds[4].SetWorldMission(2, "???");
		worlds[4].SetWorldMission(3, "???");
		worlds[4].SetWorldMission(4, "???");
		worlds[4].SetWorldMission(5, "???");
		worlds[4].SetWorldMission(6, "???");
		worlds[4].SetWorldMission(7, "???");
		worlds[4].SetWorldMission(8, "???");
		worlds[4].SetWorldMission(9, "???");
		
		//World 6: Sky Zone
		worlds[5].worldName = "Aerial Zone";
		worlds[5].likesNeeded = 27;
		worlds[5].SetWorldMission(0, "???");
		worlds[5].SetWorldMission(1, "???");
		worlds[5].SetWorldMission(2, "???");
		worlds[5].SetWorldMission(3, "???");
		worlds[5].SetWorldMission(4, "???");
		worlds[5].SetWorldMission(5, "???");
		worlds[5].SetWorldMission(6, "???");
		worlds[5].SetWorldMission(7, "???");
		worlds[5].SetWorldMission(8, "???");
		worlds[5].SetWorldMission(9, "???");
		
		//World 7: Neon City
		worlds[6].worldName = "Flashing City";
		worlds[6].likesNeeded = 32;
		worlds[6].SetWorldMission(0, "???");
		worlds[6].SetWorldMission(1, "???");
		worlds[6].SetWorldMission(2, "???");
		worlds[6].SetWorldMission(3, "???");
		worlds[6].SetWorldMission(4, "???");
		worlds[6].SetWorldMission(5, "???");
		worlds[6].SetWorldMission(6, "???");
		worlds[6].SetWorldMission(7, "???");
		worlds[6].SetWorldMission(8, "???");
		worlds[6].SetWorldMission(9, "???");
		
		//World 8: Swamp
		worlds[7].worldName = "Moist Plains";
		worlds[7].likesNeeded = 40;
		worlds[7].SetWorldMission(0, "???");
		worlds[7].SetWorldMission(1, "???");
		worlds[7].SetWorldMission(2, "???");
		worlds[7].SetWorldMission(3, "???");
		worlds[7].SetWorldMission(4, "???");
		worlds[7].SetWorldMission(5, "???");
		worlds[7].SetWorldMission(6, "???");
		worlds[7].SetWorldMission(7, "???");
		worlds[7].SetWorldMission(8, "???");
		worlds[7].SetWorldMission(9, "???");
	}
}
