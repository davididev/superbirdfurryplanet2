Shader "Skybox/Layered Skysphere" {
Properties {
    _Tint ("Layer 1 Tint", Color) = (1, 1, 1, 1)
    [Gamma] _Exposure ("Exposure", Range(0, 8)) = 1.0
    _Rotation ("L1 Rotation", Range(0, 360)) = 0
	_Rotation2 ("L2 Rotation", Range(0, 360)) = 0
	_Rotation3 ("L3 Rotation", Range(0, 360)) = 0
    [NoScaleOffset] _Tex ("Layer 1 (NOT transparent)", 2D) = "grey" {}
	_Tint2 ("Layer 2 Tint", Color) = (1, 1, 1, 1)
	[NoScaleOffset] _Tex2 ("Layer 2  (Transparent)", 2D) = "grey" {}
	_Tint3 ("Layer 3 Tint", Color) = (1, 1, 1, 1)
	[NoScaleOffset] _Tex3 ("Layer 3  (Transparent)", 2D) = "grey" {}
    
    [Enum(360 Degrees, 0, 180 Degrees, 1)] _ImageType("Image Type", Float) = 0
    [Toggle] _MirrorOnBack("Mirror on Back", Float) = 0
    [Enum(None, 0, Side by Side, 1, Over Under, 2)] _Layout("3D Layout", Float) = 0
}

SubShader {
    Tags { "Queue"="Background" "RenderType"="Background" "PreviewType"="Skybox" }
    Cull Off ZWrite Off

    Pass {

        CGPROGRAM
        #pragma vertex vert
        #pragma fragment frag
        #pragma target 2.0
        

        #include "UnityCG.cginc"

        sampler2D _Tex;
		sampler2D _Tex2;
		sampler2D _Tex3;
        float4 _Tex_TexelSize;
        half4 _Tex_HDR;
        half4 _Tint;
		half4 _Tint2;
		half4 _Tint3;
        half _Exposure;
        float _Rotation;
		float _Rotation2;
		float _Rotation3;
		bool _MirrorOnBack;
        int _ImageType;
        int _Layout;


        inline float2 ToRadialCoords(float3 coords)
        {
            float3 normalizedCoords = normalize(coords);
            float latitude = acos(normalizedCoords.y);
            float longitude = atan2(normalizedCoords.z, normalizedCoords.x);
            float2 sphereCoords = float2(longitude, latitude) * float2(0.5/UNITY_PI, 1.0/UNITY_PI);
            return float2(0.5,1.0) - sphereCoords;
        }

        float3 RotateAroundYInDegrees (float3 vertex, float degrees)
        {
            float alpha = degrees * UNITY_PI / 180.0;
            float sina, cosa;
            sincos(alpha, sina, cosa);
            float2x2 m = float2x2(cosa, -sina, sina, cosa);
            return float3(mul(m, vertex.xz), vertex.y).xzy;
        }

        struct appdata_t {
            float4 vertex : POSITION;
            UNITY_VERTEX_INPUT_INSTANCE_ID
        };

        struct v2f {
            float4 vertex : SV_POSITION;
            float3 texcoord : TEXCOORD0;

            float2 image180ScaleAndCutoff : TEXCOORD1;
            float4 layout3DScaleAndOffset : TEXCOORD2;

            UNITY_VERTEX_OUTPUT_STEREO
        };

        v2f vert (appdata_t v)
        {
            v2f o;
            UNITY_SETUP_INSTANCE_ID(v);
            UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
            float3 rotated = RotateAroundYInDegrees(v.vertex, _Rotation);
			float3 rotated2 = RotateAroundYInDegrees(v.vertex, _Rotation2);
			float3 rotated3 = RotateAroundYInDegrees(v.vertex, _Rotation3);
            o.vertex = UnityObjectToClipPos(rotated);
            o.texcoord = v.vertex.xyz;

            // Calculate constant horizontal scale and cutoff for 180 (vs 360) image type
            if (_ImageType == 0)  // 360 degree
                o.image180ScaleAndCutoff = float2(1.0, 1.0);
            else  // 180 degree
                o.image180ScaleAndCutoff = float2(2.0, _MirrorOnBack ? 1.0 : 0.5);
            // Calculate constant scale and offset for 3D layouts
            if (_Layout == 0) // No 3D layout
                o.layout3DScaleAndOffset = float4(0,0,1,1);
            else if (_Layout == 1) // Side-by-Side 3D layout
                o.layout3DScaleAndOffset = float4(unity_StereoEyeIndex,0,0.5,1);
            else // Over-Under 3D layout
                o.layout3DScaleAndOffset = float4(0, 1-unity_StereoEyeIndex,1,0.5);

            return o;
        }

        fixed4 frag (v2f i) : SV_Target
        {
            float2 tc = ToRadialCoords(i.texcoord);
            if (tc.x > i.image180ScaleAndCutoff[1])
                return half4(0,0,0,1);
            tc.x = fmod(tc.x*i.image180ScaleAndCutoff[0], 1);
            tc = (tc + i.layout3DScaleAndOffset.xy) * i.layout3DScaleAndOffset.zw;
			float2 tc2 = tc;
			tc2.x += (_Rotation2 - _Rotation) / 360.0;
			float2 tc3 = tc;
			tc3.x += (_Rotation3 - _Rotation) / 360.0;
			

            half4 tex = tex2D (_Tex, tc);
			half4 tex2 = tex2D (_Tex2, tc2);
			half4 tex3 = tex2D (_Tex3, tc3);
            half3 c = DecodeHDR (tex, _Tex_HDR);
            c = c * _Tint.rgb * unity_ColorSpaceDouble.rgb;
            c *= _Exposure;
			half3 c2 = DecodeHDR(tex2, _Tex_HDR);
			half3 c3 = DecodeHDR(tex3, _Tex_HDR);
			c2 = c2 * _Tint2.rgb * unity_ColorSpaceDouble.rgb;
			c2 *= _Exposure;
			c3 = c3 * _Tint3.rgb * unity_ColorSpaceDouble.rgb;
			c3 *= _Exposure;
			
			fixed4 d1 = fixed4(c, tex.a * _Tint.a); 
			fixed4 d2 = fixed4(c2, tex2.a * _Tint2.a);
			fixed4 d3 = fixed4(c3, tex3.a * _Tint3.a);
			
			//half3 result = (c * _Tint) + (c2 * tex2.a * _Tint2) + (c3 * tex3.a * _Tint3);
			fixed4 e1 = lerp(d1, d2, d2.a);
			fixed4 e2 = lerp(d2, d3, d3.a);
			fixed4 result = lerp(e1, e2, e2.a);
			
            //return half4(result, 1);
			return result;
        }
        ENDCG
    }
}

Fallback Off

}
