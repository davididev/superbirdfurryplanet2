﻿using System.Collections;
using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

public class ScreenshotHandler : MonoBehaviour
{
    public TMPro.TextMeshProUGUI screenshotText;
    private float screenshotTextTimer = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(screenshotTextTimer > 0f)
        {
            screenshotTextTimer -= Time.deltaTime;
            if (screenshotTextTimer < 0f)
                screenshotText.text = "";
        }
    }

    public void SaveScreenshot()
    {
        screenshotTextTimer = 1f;
        screenshotText.text = "";
        DateTime currentDate = new DateTime();
        currentDate = DateTime.Now;
        string fileName = currentDate.ToShortDateString().Replace("/", "-") + " " + currentDate.ToShortTimeString().Replace(":", "-") + currentDate.Millisecond.ToString("D4");



        StartCoroutine(Screenshot(fileName));


    }

    bool error = false;
    void Error(string s)
    {
        if (s == null)
        {
            OnScreenshot(true);
            return;
        }
        error = true;
        //OnScreenshot(false);
        screenshotText.text = "<color=red>" + s + "</color>";
    }

    private void OnScreenshot(bool val)
    {

        string dest = "My Pictures";
        if (Application.isMobilePlatform)
            dest = "Photo Gallery";
        if (val == true)
            screenshotText.text = "Screenshot saved successfully to " + dest;
        else
            screenshotText.text = "<color=red>Screenshot failed to save</color>";
    }
    private IEnumerator Screenshot(string fileName)
    {

        yield return new WaitForEndOfFrame();

        Texture2D img = ScreenCapture.CaptureScreenshotAsTexture(ScreenCapture.StereoScreenCaptureMode.BothEyes);
        
		while(img == null)
		{
			yield return new WaitForSecondsRealtime(0.05f);
		}
		
        if (Application.isMobilePlatform)
        {
            
            //Upload image to android gallery
            NativeGallery.SaveImageToGallery(img, "Superbird", fileName, Error);

        }
        else
        {
            //Upload image to myPictures
            string oldDir = Application.persistentDataPath;
            string newDir = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            byte[] file = img.EncodeToJPG();
            try
            {
                System.IO.File.WriteAllBytes(oldDir + "/" + fileName + ".jpg", file);
            }
            catch (Exception e)
            {
                Error("Error: " + e.ToString());
            }

            while (File.Exists(oldDir + "/" + fileName + ".jpg") == false)
            {
                yield return new WaitForSecondsRealtime(0.05f);
            }

            try
            {
                System.IO.File.Move(oldDir + "/" + fileName + ".jpg", newDir + "/" + fileName + ".jpg");
                Error(null);
            }
            catch (Exception e)
            {
                Error("Error: " + e.ToString());
            }

        }

        yield return new WaitForSecondsRealtime(0.05f);

    }
}
